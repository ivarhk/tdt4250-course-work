# TDT4250 Advanced Software Design &mdash; Assignments

**Ivar H. Kråbøl [ivarhk@stud.ntnu.no]**

## Case

The case assignment(s) include the following directories:

### [`hk.ivar.tdt4250.case`](hk.ivar.tdt4250.case/)

Contains the ecore model ([`model/case.ecore`](hk.ivar.tdt4250.case/model/case.ecore)), the genmodel
([`model/case.genmodel`](hk.ivar.tdt4250.case/model/case.genmodel)), and the generated source code
for the model ([`src/case_`](hk.ivar.tdt4250.case/src/case_), as well as a dynamic instance of the
model ([`model/case.department.idi.xmi`](hk.ivar.tdt4250.case/model/case.department.idi.xmi)).

### [`hk.ivar.tdt4250.case.transformation`](hk.ivar.tdt4250.case.transformation/)

Contains the ATL transformation project

### [`hk.ivar.tdt4250.html5`](hk.ivar.tdt4250.html5/)

Contains my own less than barebones model of html5, so that I could use M2M from *case* to *html5*,
and then serialize. I could have used the xhtml1 project, but had some problems getting it working.

## Testing the transformation

### Requirements

In order to run the ATL transformation, you need to install the *ATL - ATL Transformation Language*
plug-in from the standard Eclipse software site.

### Launching

The repo contains two `*.launch` files, which should be launched from Eclipse in order:

1. [`hk.ivar.tdt4250.case.transformation/case2html5.launch`](hk.ivar.tdt4250.case.transformation/case2html5.launch),
which uses the *case* ecore model as input metamodel and the html5 ecore model ([`hk.ivar.tdt4250.html5/model/html5.ecore`](hk.ivar.tdt4250.html5/model/html5.ecore))
as output metamodel, and transforms the *case* instance file ([`hk.ivar.tdt4250.case/model/case.department.idi.xmi`](hk.ivar.tdt4250.case/model/case.department.idi.xmi))
into an *html5* instance file ([`hk.ivar.tdt4250.html5/model/generated/html5.department.idi.xmi`](hk.ivar.tdt4250.html5/model/generated/html5.department.idi.xmi))

2. [`hk.ivar.tdt4250.html5/Html5Main.launch`](hk.ivar.tdt4250.html5/Html5Main.launch), which
serializes the *html5* instance file ([`hk.ivar.tdt4250.html5/model/generated/html5.department.idi.xmi`](hk.ivar.tdt4250.html5/model/generated/html5.department.idi.xmi))
and stores the content in an html file ([`hk.ivar.tdt4250.html5/model/generated/html5.department.idi.html`](hk.ivar.tdt4250.html5/model/generated/html5.department.idi.html))
