package html5.main;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.Diagnostician;

import html5.Html5Package;
import html5.HtmlTag;
import html5.util.Html5ResourceFactoryImpl;
import html5.util.Html5Serializer;;

public class Html5Main {

	private static void registerEMFStuff() {
		// registers the model's package object on its unique URI
		EPackage.Registry.INSTANCE.put(Html5Package.eNS_PREFIX, Html5Package.eINSTANCE);
		// register our Resource factory for the xmi extension, so the right Resource implementation is used
		Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put("xmi", new Html5ResourceFactoryImpl());
	}
	
	public static void main(String[] args) {
		URI resourceUri;
		PrintStream out;

		if (args.length >= 1) resourceUri = URI.createURI(args[0]);
		else resourceUri = URI.createURI("model/html5.html.test1.xmi");
		
		PrintStream outFileStream = null;
		
		if (args.length >= 2) {
			try {
				File outFile = new File(args[1]);
				outFile.createNewFile();
				outFileStream = new PrintStream(outFile);
			} catch (IOException e) {
				e.printStackTrace(System.err);
				return;
			} 
			out = outFileStream;
		} else {
			out = System.out;
		}

		registerEMFStuff();

		ResourceSet resourceSet = new ResourceSetImpl();
		Resource resource = resourceSet.getResource(resourceUri, true);
		try {
			HtmlTag root = (HtmlTag)resource.getContents().get(0);
			Diagnostic diagnostics = Diagnostician.INSTANCE.validate(root);
			if (diagnostics.getSeverity() != Diagnostic.OK) {
				System.err.println(diagnostics.getMessage());
				for (Diagnostic child : diagnostics.getChildren()) {
					System.err.println(child.getMessage());
				}
			} else {
				System.err.println("No problems with " + root);
			}
			out.println(Html5Serializer.serialize(root));
		} catch (RuntimeException e) {
			e.printStackTrace(System.err);
		} finally {
			if (outFileStream != null) {
				outFileStream.close();
			}
		}
	}
}
