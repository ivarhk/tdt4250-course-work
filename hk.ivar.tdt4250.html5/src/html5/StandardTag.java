/**
 */
package html5;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Standard Tag</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link html5.StandardTag#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see html5.Html5Package#getStandardTag()
 * @model
 * @generated
 */
public interface StandardTag extends CommonTagParentInterface, CommonTagInterface {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The literals are from the enumeration {@link html5.StandardTagType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see html5.StandardTagType
	 * @see #setType(StandardTagType)
	 * @see html5.Html5Package#getStandardTag_Type()
	 * @model required="true"
	 * @generated
	 */
	StandardTagType getType();

	/**
	 * Sets the value of the '{@link html5.StandardTag#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see html5.StandardTagType
	 * @see #getType()
	 * @generated
	 */
	void setType(StandardTagType value);

} // StandardTag
