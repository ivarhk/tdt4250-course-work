/**
 */
package html5;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Title Tag</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link html5.TitleTag#getParent <em>Parent</em>}</li>
 * </ul>
 *
 * @see html5.Html5Package#getTitleTag()
 * @model
 * @generated
 */
public interface TitleTag extends TagInterface {
	/**
	 * Returns the value of the '<em><b>Parent</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link html5.HeadTag#getTitle <em>Title</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent</em>' container reference.
	 * @see #setParent(HeadTag)
	 * @see html5.Html5Package#getTitleTag_Parent()
	 * @see html5.HeadTag#getTitle
	 * @model opposite="title" transient="false"
	 * @generated
	 */
	HeadTag getParent();

	/**
	 * Sets the value of the '{@link html5.TitleTag#getParent <em>Parent</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent</em>' container reference.
	 * @see #getParent()
	 * @generated
	 */
	void setParent(HeadTag value);

} // TitleTag
