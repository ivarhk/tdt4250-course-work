/**
 */
package html5;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Tag Interface</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link html5.TagInterface#getTagName <em>Tag Name</em>}</li>
 *   <li>{@link html5.TagInterface#getText <em>Text</em>}</li>
 *   <li>{@link html5.TagInterface#getTail <em>Tail</em>}</li>
 * </ul>
 *
 * @see html5.Html5Package#getTagInterface()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface TagInterface extends EObject {
	/**
	 * Returns the value of the '<em><b>Tag Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tag Name</em>' attribute.
	 * @see html5.Html5Package#getTagInterface_TagName()
	 * @model transient="true" changeable="false" volatile="true"
	 * @generated
	 */
	String getTagName();

	/**
	 * Returns the value of the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Text</em>' attribute.
	 * @see #setText(String)
	 * @see html5.Html5Package#getTagInterface_Text()
	 * @model
	 * @generated
	 */
	String getText();

	/**
	 * Sets the value of the '{@link html5.TagInterface#getText <em>Text</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Text</em>' attribute.
	 * @see #getText()
	 * @generated
	 */
	void setText(String value);

	/**
	 * Returns the value of the '<em><b>Tail</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tail</em>' attribute.
	 * @see #setTail(String)
	 * @see html5.Html5Package#getTagInterface_Tail()
	 * @model
	 * @generated
	 */
	String getTail();

	/**
	 * Sets the value of the '{@link html5.TagInterface#getTail <em>Tail</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Tail</em>' attribute.
	 * @see #getTail()
	 * @generated
	 */
	void setTail(String value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	TagInterface getParentTag();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	EList<EAttribute> getHtmlAttributes();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	EList<TagInterface> getChildTags();

} // TagInterface
