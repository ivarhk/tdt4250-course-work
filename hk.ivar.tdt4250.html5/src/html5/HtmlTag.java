/**
 */
package html5;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Html Tag</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link html5.HtmlTag#getBody <em>Body</em>}</li>
 *   <li>{@link html5.HtmlTag#getHead <em>Head</em>}</li>
 * </ul>
 *
 * @see html5.Html5Package#getHtmlTag()
 * @model
 * @generated
 */
public interface HtmlTag extends TagInterface {
	/**
	 * Returns the value of the '<em><b>Body</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link html5.BodyTag#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Body</em>' containment reference.
	 * @see #setBody(BodyTag)
	 * @see html5.Html5Package#getHtmlTag_Body()
	 * @see html5.BodyTag#getParent
	 * @model opposite="parent" containment="true"
	 * @generated
	 */
	BodyTag getBody();

	/**
	 * Sets the value of the '{@link html5.HtmlTag#getBody <em>Body</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Body</em>' containment reference.
	 * @see #getBody()
	 * @generated
	 */
	void setBody(BodyTag value);

	/**
	 * Returns the value of the '<em><b>Head</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link html5.HeadTag#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Head</em>' containment reference.
	 * @see #setHead(HeadTag)
	 * @see html5.Html5Package#getHtmlTag_Head()
	 * @see html5.HeadTag#getParent
	 * @model opposite="parent" containment="true"
	 * @generated
	 */
	HeadTag getHead();

	/**
	 * Sets the value of the '{@link html5.HtmlTag#getHead <em>Head</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Head</em>' containment reference.
	 * @see #getHead()
	 * @generated
	 */
	void setHead(HeadTag value);

} // HtmlTag
