/**
 */
package html5;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TSection Tag</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link html5.TSectionTag#getParent <em>Parent</em>}</li>
 *   <li>{@link html5.TSectionTag#isThead <em>Thead</em>}</li>
 *   <li>{@link html5.TSectionTag#getRows <em>Rows</em>}</li>
 * </ul>
 *
 * @see html5.Html5Package#getTSectionTag()
 * @model
 * @generated
 */
public interface TSectionTag extends TagInterface {
	/**
	 * Returns the value of the '<em><b>Parent</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link html5.TableTag#getTsections <em>Tsections</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent</em>' container reference.
	 * @see #setParent(TableTag)
	 * @see html5.Html5Package#getTSectionTag_Parent()
	 * @see html5.TableTag#getTsections
	 * @model opposite="tsections" transient="false"
	 * @generated
	 */
	TableTag getParent();

	/**
	 * Sets the value of the '{@link html5.TSectionTag#getParent <em>Parent</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent</em>' container reference.
	 * @see #getParent()
	 * @generated
	 */
	void setParent(TableTag value);

	/**
	 * Returns the value of the '<em><b>Thead</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Thead</em>' attribute.
	 * @see #setThead(boolean)
	 * @see html5.Html5Package#getTSectionTag_Thead()
	 * @model unique="false" id="true"
	 * @generated
	 */
	boolean isThead();

	/**
	 * Sets the value of the '{@link html5.TSectionTag#isThead <em>Thead</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Thead</em>' attribute.
	 * @see #isThead()
	 * @generated
	 */
	void setThead(boolean value);

	/**
	 * Returns the value of the '<em><b>Rows</b></em>' containment reference list.
	 * The list contents are of type {@link html5.TrTag}.
	 * It is bidirectional and its opposite is '{@link html5.TrTag#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rows</em>' containment reference list.
	 * @see html5.Html5Package#getTSectionTag_Rows()
	 * @see html5.TrTag#getParent
	 * @model opposite="parent" containment="true"
	 * @generated
	 */
	EList<TrTag> getRows();

} // TSectionTag
