/**
 */
package html5;

import org.eclipse.emf.common.util.EList;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Table Tag</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link html5.TableTag#getThead <em>Thead</em>}</li>
 *   <li>{@link html5.TableTag#getTbody <em>Tbody</em>}</li>
 *   <li>{@link html5.TableTag#getTsections <em>Tsections</em>}</li>
 * </ul>
 *
 * @see html5.Html5Package#getTableTag()
 * @model
 * @generated
 */
public interface TableTag extends CommonTagInterface {
	/**
	 * Returns the value of the '<em><b>Thead</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Thead</em>' reference.
	 * @see html5.Html5Package#getTableTag_Thead()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	TsectionTag getThead();

	/**
	 * Returns the value of the '<em><b>Tbody</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tbody</em>' reference.
	 * @see html5.Html5Package#getTableTag_Tbody()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	TsectionTag getTbody();

	/**
	 * Returns the value of the '<em><b>Tsections</b></em>' containment reference list.
	 * The list contents are of type {@link html5.TsectionTag}.
	 * It is bidirectional and its opposite is '{@link html5.TsectionTag#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tsections</em>' containment reference list.
	 * @see html5.Html5Package#getTableTag_Tsections()
	 * @see html5.TsectionTag#getParent
	 * @model opposite="parent" containment="true" upper="2" ordered="false"
	 * @generated
	 */
	EList<TsectionTag> getTsections();

} // TableTag
