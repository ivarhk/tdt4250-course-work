/**
 */
package html5.impl;

import html5.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class Html5FactoryImpl extends EFactoryImpl implements Html5Factory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Html5Factory init() {
		try {
			Html5Factory theHtml5Factory = (Html5Factory)EPackage.Registry.INSTANCE.getEFactory(Html5Package.eNS_URI);
			if (theHtml5Factory != null) {
				return theHtml5Factory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new Html5FactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Html5FactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case Html5Package.HTML_TAG: return createHtmlTag();
			case Html5Package.HEAD_TAG: return createHeadTag();
			case Html5Package.TITLE_TAG: return createTitleTag();
			case Html5Package.BODY_TAG: return createBodyTag();
			case Html5Package.STANDARD_TAG: return createStandardTag();
			case Html5Package.ATAG: return createATag();
			case Html5Package.TABLE_TAG: return createTableTag();
			case Html5Package.TSECTION_TAG: return createTsectionTag();
			case Html5Package.TR_TAG: return createTrTag();
			case Html5Package.TCELL_TAG: return createTCellTag();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case Html5Package.STANDARD_TAG_TYPE:
				return createStandardTagTypeFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case Html5Package.STANDARD_TAG_TYPE:
				return convertStandardTagTypeToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HtmlTag createHtmlTag() {
		HtmlTagImpl htmlTag = new HtmlTagImpl();
		return htmlTag;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HeadTag createHeadTag() {
		HeadTagImpl headTag = new HeadTagImpl();
		return headTag;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TitleTag createTitleTag() {
		TitleTagImpl titleTag = new TitleTagImpl();
		return titleTag;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BodyTag createBodyTag() {
		BodyTagImpl bodyTag = new BodyTagImpl();
		return bodyTag;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StandardTag createStandardTag() {
		StandardTagImpl standardTag = new StandardTagImpl();
		return standardTag;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ATag createATag() {
		ATagImpl aTag = new ATagImpl();
		return aTag;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TableTag createTableTag() {
		TableTagImpl tableTag = new TableTagImpl();
		return tableTag;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TsectionTag createTsectionTag() {
		TsectionTagImpl tsectionTag = new TsectionTagImpl();
		return tsectionTag;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TrTag createTrTag() {
		TrTagImpl trTag = new TrTagImpl();
		return trTag;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TCellTag createTCellTag() {
		TCellTagImpl tCellTag = new TCellTagImpl();
		return tCellTag;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StandardTagType createStandardTagTypeFromString(EDataType eDataType, String initialValue) {
		StandardTagType result = StandardTagType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertStandardTagTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Html5Package getHtml5Package() {
		return (Html5Package)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static Html5Package getPackage() {
		return Html5Package.eINSTANCE;
	}

} //Html5FactoryImpl
