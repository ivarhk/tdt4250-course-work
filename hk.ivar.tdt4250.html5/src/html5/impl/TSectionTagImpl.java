/**
 */
package html5.impl;

import html5.Html5Package;
import html5.TSectionTag;
import html5.TableTag;
import html5.TagInterface;
import html5.TrTag;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>TSection Tag</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link html5.impl.TSectionTagImpl#getTagName <em>Tag Name</em>}</li>
 *   <li>{@link html5.impl.TSectionTagImpl#getText <em>Text</em>}</li>
 *   <li>{@link html5.impl.TSectionTagImpl#getTail <em>Tail</em>}</li>
 *   <li>{@link html5.impl.TSectionTagImpl#getParent <em>Parent</em>}</li>
 *   <li>{@link html5.impl.TSectionTagImpl#isThead <em>Thead</em>}</li>
 *   <li>{@link html5.impl.TSectionTagImpl#getRows <em>Rows</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TSectionTagImpl extends MinimalEObjectImpl.Container implements TSectionTag {
	/**
	 * The default value of the '{@link #getTagName() <em>Tag Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTagName()
	 * @generated
	 * @ordered
	 */
	protected static final String TAG_NAME_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getText() <em>Text</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getText()
	 * @generated
	 * @ordered
	 */
	protected static final String TEXT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getText() <em>Text</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getText()
	 * @generated
	 * @ordered
	 */
	protected String text = TEXT_EDEFAULT;

	/**
	 * The default value of the '{@link #getTail() <em>Tail</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTail()
	 * @generated
	 * @ordered
	 */
	protected static final String TAIL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTail() <em>Tail</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTail()
	 * @generated
	 * @ordered
	 */
	protected String tail = TAIL_EDEFAULT;

	/**
	 * The default value of the '{@link #isThead() <em>Thead</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isThead()
	 * @generated
	 * @ordered
	 */
	protected static final boolean THEAD_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isThead() <em>Thead</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isThead()
	 * @generated
	 * @ordered
	 */
	protected boolean thead = THEAD_EDEFAULT;

	/**
	 * The cached value of the '{@link #getRows() <em>Rows</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRows()
	 * @generated
	 * @ordered
	 */
	protected EList<TrTag> rows;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TSectionTagImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Html5Package.Literals.TSECTION_TAG;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public String getTagName() {
		return isThead() ? "thead" : "tbody";
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getText() {
		return text;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setText(String newText) {
		String oldText = text;
		text = newText;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Html5Package.TSECTION_TAG__TEXT, oldText, text));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTail() {
		return tail;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTail(String newTail) {
		String oldTail = tail;
		tail = newTail;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Html5Package.TSECTION_TAG__TAIL, oldTail, tail));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TableTag getParent() {
		if (eContainerFeatureID() != Html5Package.TSECTION_TAG__PARENT) return null;
		return (TableTag)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParent(TableTag newParent, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newParent, Html5Package.TSECTION_TAG__PARENT, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParent(TableTag newParent) {
		if (newParent != eInternalContainer() || (eContainerFeatureID() != Html5Package.TSECTION_TAG__PARENT && newParent != null)) {
			if (EcoreUtil.isAncestor(this, newParent))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newParent != null)
				msgs = ((InternalEObject)newParent).eInverseAdd(this, Html5Package.TABLE_TAG__TSECTIONS, TableTag.class, msgs);
			msgs = basicSetParent(newParent, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Html5Package.TSECTION_TAG__PARENT, newParent, newParent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean isThead() {
		return getParent().getThead() == this;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setThead(boolean newThead) {
		boolean oldThead = thead;
		thead = newThead;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Html5Package.TSECTION_TAG__THEAD, oldThead, thead));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TrTag> getRows() {
		if (rows == null) {
			rows = new EObjectContainmentWithInverseEList<TrTag>(TrTag.class, this, Html5Package.TSECTION_TAG__ROWS, Html5Package.TR_TAG__PARENT);
		}
		return rows;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public TagInterface getParentTag() {
		return getParent();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<EAttribute> getHtmlAttributes() {
		return new BasicEList<EAttribute>();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<TagInterface> getChildTags() {
		return new BasicEList<TagInterface>(rows);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Html5Package.TSECTION_TAG__PARENT:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetParent((TableTag)otherEnd, msgs);
			case Html5Package.TSECTION_TAG__ROWS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getRows()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Html5Package.TSECTION_TAG__PARENT:
				return basicSetParent(null, msgs);
			case Html5Package.TSECTION_TAG__ROWS:
				return ((InternalEList<?>)getRows()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case Html5Package.TSECTION_TAG__PARENT:
				return eInternalContainer().eInverseRemove(this, Html5Package.TABLE_TAG__TSECTIONS, TableTag.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Html5Package.TSECTION_TAG__TAG_NAME:
				return getTagName();
			case Html5Package.TSECTION_TAG__TEXT:
				return getText();
			case Html5Package.TSECTION_TAG__TAIL:
				return getTail();
			case Html5Package.TSECTION_TAG__PARENT:
				return getParent();
			case Html5Package.TSECTION_TAG__THEAD:
				return isThead();
			case Html5Package.TSECTION_TAG__ROWS:
				return getRows();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Html5Package.TSECTION_TAG__TEXT:
				setText((String)newValue);
				return;
			case Html5Package.TSECTION_TAG__TAIL:
				setTail((String)newValue);
				return;
			case Html5Package.TSECTION_TAG__PARENT:
				setParent((TableTag)newValue);
				return;
			case Html5Package.TSECTION_TAG__THEAD:
				setThead((Boolean)newValue);
				return;
			case Html5Package.TSECTION_TAG__ROWS:
				getRows().clear();
				getRows().addAll((Collection<? extends TrTag>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Html5Package.TSECTION_TAG__TEXT:
				setText(TEXT_EDEFAULT);
				return;
			case Html5Package.TSECTION_TAG__TAIL:
				setTail(TAIL_EDEFAULT);
				return;
			case Html5Package.TSECTION_TAG__PARENT:
				setParent((TableTag)null);
				return;
			case Html5Package.TSECTION_TAG__THEAD:
				setThead(THEAD_EDEFAULT);
				return;
			case Html5Package.TSECTION_TAG__ROWS:
				getRows().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Html5Package.TSECTION_TAG__TAG_NAME:
				return TAG_NAME_EDEFAULT == null ? getTagName() != null : !TAG_NAME_EDEFAULT.equals(getTagName());
			case Html5Package.TSECTION_TAG__TEXT:
				return TEXT_EDEFAULT == null ? text != null : !TEXT_EDEFAULT.equals(text);
			case Html5Package.TSECTION_TAG__TAIL:
				return TAIL_EDEFAULT == null ? tail != null : !TAIL_EDEFAULT.equals(tail);
			case Html5Package.TSECTION_TAG__PARENT:
				return getParent() != null;
			case Html5Package.TSECTION_TAG__THEAD:
				return thead != THEAD_EDEFAULT;
			case Html5Package.TSECTION_TAG__ROWS:
				return rows != null && !rows.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case Html5Package.TSECTION_TAG___GET_PARENT_TAG:
				return getParentTag();
			case Html5Package.TSECTION_TAG___GET_HTML_ATTRIBUTES:
				return getHtmlAttributes();
			case Html5Package.TSECTION_TAG___GET_CHILD_TAGS:
				return getChildTags();
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (text: ");
		result.append(text);
		result.append(", tail: ");
		result.append(tail);
		result.append(", thead: ");
		result.append(thead);
		result.append(')');
		return result.toString();
	}

} //TSectionTagImpl
