/**
 */
package html5.impl;

import html5.ATag;
import html5.BodyTag;
import html5.CommonTagInterface;
import html5.CommonTagParentInterface;
import html5.HeadTag;
import html5.Html5Factory;
import html5.Html5Package;
import html5.HtmlTag;
import html5.StandardTag;
import html5.StandardTagType;
import html5.TCellTag;
import html5.TableTag;
import html5.TagInterface;
import html5.TitleTag;
import html5.TrTag;

import html5.TsectionTag;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class Html5PackageImpl extends EPackageImpl implements Html5Package {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tagInterfaceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass commonTagParentInterfaceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass commonTagInterfaceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass htmlTagEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass headTagEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass titleTagEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bodyTagEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass standardTagEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aTagEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tableTagEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tsectionTagEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass trTagEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tCellTagEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum standardTagTypeEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see html5.Html5Package#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private Html5PackageImpl() {
		super(eNS_URI, Html5Factory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link Html5Package#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static Html5Package init() {
		if (isInited) return (Html5Package)EPackage.Registry.INSTANCE.getEPackage(Html5Package.eNS_URI);

		// Obtain or create and register package
		Object registeredHtml5Package = EPackage.Registry.INSTANCE.get(eNS_URI);
		Html5PackageImpl theHtml5Package = registeredHtml5Package instanceof Html5PackageImpl ? (Html5PackageImpl)registeredHtml5Package : new Html5PackageImpl();

		isInited = true;

		// Create package meta-data objects
		theHtml5Package.createPackageContents();

		// Initialize created meta-data
		theHtml5Package.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theHtml5Package.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(Html5Package.eNS_URI, theHtml5Package);
		return theHtml5Package;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTagInterface() {
		return tagInterfaceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTagInterface_TagName() {
		return (EAttribute)tagInterfaceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTagInterface_Text() {
		return (EAttribute)tagInterfaceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTagInterface_Tail() {
		return (EAttribute)tagInterfaceEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getTagInterface__GetParentTag() {
		return tagInterfaceEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getTagInterface__GetHtmlAttributes() {
		return tagInterfaceEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getTagInterface__GetChildTags() {
		return tagInterfaceEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCommonTagParentInterface() {
		return commonTagParentInterfaceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCommonTagParentInterface_Children() {
		return (EReference)commonTagParentInterfaceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCommonTagInterface() {
		return commonTagInterfaceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCommonTagInterface_Parent() {
		return (EReference)commonTagInterfaceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHtmlTag() {
		return htmlTagEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHtmlTag_Body() {
		return (EReference)htmlTagEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHtmlTag_Head() {
		return (EReference)htmlTagEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHeadTag() {
		return headTagEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHeadTag_Parent() {
		return (EReference)headTagEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHeadTag_Title() {
		return (EReference)headTagEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTitleTag() {
		return titleTagEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTitleTag_Parent() {
		return (EReference)titleTagEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBodyTag() {
		return bodyTagEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBodyTag_Parent() {
		return (EReference)bodyTagEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStandardTag() {
		return standardTagEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getStandardTag_Type() {
		return (EAttribute)standardTagEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getATag() {
		return aTagEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getATag_Href() {
		return (EAttribute)aTagEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getATag_Title() {
		return (EAttribute)aTagEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTableTag() {
		return tableTagEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTableTag_Thead() {
		return (EReference)tableTagEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTableTag_Tbody() {
		return (EReference)tableTagEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTableTag_Tsections() {
		return (EReference)tableTagEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTsectionTag() {
		return tsectionTagEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTsectionTag_Parent() {
		return (EReference)tsectionTagEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTsectionTag_Thead() {
		return (EAttribute)tsectionTagEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTsectionTag_Rows() {
		return (EReference)tsectionTagEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTrTag() {
		return trTagEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTrTag_Parent() {
		return (EReference)trTagEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTrTag_Cells() {
		return (EReference)trTagEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTCellTag() {
		return tCellTagEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTCellTag_Parent() {
		return (EReference)tCellTagEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTCellTag_Th() {
		return (EAttribute)tCellTagEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getStandardTagType() {
		return standardTagTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Html5Factory getHtml5Factory() {
		return (Html5Factory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		tagInterfaceEClass = createEClass(TAG_INTERFACE);
		createEAttribute(tagInterfaceEClass, TAG_INTERFACE__TAG_NAME);
		createEAttribute(tagInterfaceEClass, TAG_INTERFACE__TEXT);
		createEAttribute(tagInterfaceEClass, TAG_INTERFACE__TAIL);
		createEOperation(tagInterfaceEClass, TAG_INTERFACE___GET_PARENT_TAG);
		createEOperation(tagInterfaceEClass, TAG_INTERFACE___GET_HTML_ATTRIBUTES);
		createEOperation(tagInterfaceEClass, TAG_INTERFACE___GET_CHILD_TAGS);

		commonTagParentInterfaceEClass = createEClass(COMMON_TAG_PARENT_INTERFACE);
		createEReference(commonTagParentInterfaceEClass, COMMON_TAG_PARENT_INTERFACE__CHILDREN);

		commonTagInterfaceEClass = createEClass(COMMON_TAG_INTERFACE);
		createEReference(commonTagInterfaceEClass, COMMON_TAG_INTERFACE__PARENT);

		htmlTagEClass = createEClass(HTML_TAG);
		createEReference(htmlTagEClass, HTML_TAG__BODY);
		createEReference(htmlTagEClass, HTML_TAG__HEAD);

		headTagEClass = createEClass(HEAD_TAG);
		createEReference(headTagEClass, HEAD_TAG__PARENT);
		createEReference(headTagEClass, HEAD_TAG__TITLE);

		titleTagEClass = createEClass(TITLE_TAG);
		createEReference(titleTagEClass, TITLE_TAG__PARENT);

		bodyTagEClass = createEClass(BODY_TAG);
		createEReference(bodyTagEClass, BODY_TAG__PARENT);

		standardTagEClass = createEClass(STANDARD_TAG);
		createEAttribute(standardTagEClass, STANDARD_TAG__TYPE);

		aTagEClass = createEClass(ATAG);
		createEAttribute(aTagEClass, ATAG__HREF);
		createEAttribute(aTagEClass, ATAG__TITLE);

		tableTagEClass = createEClass(TABLE_TAG);
		createEReference(tableTagEClass, TABLE_TAG__THEAD);
		createEReference(tableTagEClass, TABLE_TAG__TBODY);
		createEReference(tableTagEClass, TABLE_TAG__TSECTIONS);

		tsectionTagEClass = createEClass(TSECTION_TAG);
		createEReference(tsectionTagEClass, TSECTION_TAG__PARENT);
		createEAttribute(tsectionTagEClass, TSECTION_TAG__THEAD);
		createEReference(tsectionTagEClass, TSECTION_TAG__ROWS);

		trTagEClass = createEClass(TR_TAG);
		createEReference(trTagEClass, TR_TAG__PARENT);
		createEReference(trTagEClass, TR_TAG__CELLS);

		tCellTagEClass = createEClass(TCELL_TAG);
		createEReference(tCellTagEClass, TCELL_TAG__PARENT);
		createEAttribute(tCellTagEClass, TCELL_TAG__TH);

		// Create enums
		standardTagTypeEEnum = createEEnum(STANDARD_TAG_TYPE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		commonTagParentInterfaceEClass.getESuperTypes().add(this.getTagInterface());
		commonTagInterfaceEClass.getESuperTypes().add(this.getTagInterface());
		htmlTagEClass.getESuperTypes().add(this.getTagInterface());
		headTagEClass.getESuperTypes().add(this.getTagInterface());
		titleTagEClass.getESuperTypes().add(this.getTagInterface());
		bodyTagEClass.getESuperTypes().add(this.getCommonTagParentInterface());
		standardTagEClass.getESuperTypes().add(this.getCommonTagParentInterface());
		standardTagEClass.getESuperTypes().add(this.getCommonTagInterface());
		aTagEClass.getESuperTypes().add(this.getCommonTagParentInterface());
		aTagEClass.getESuperTypes().add(this.getCommonTagInterface());
		tableTagEClass.getESuperTypes().add(this.getCommonTagInterface());
		tsectionTagEClass.getESuperTypes().add(this.getTagInterface());
		trTagEClass.getESuperTypes().add(this.getTagInterface());
		tCellTagEClass.getESuperTypes().add(this.getCommonTagParentInterface());

		// Initialize classes, features, and operations; add parameters
		initEClass(tagInterfaceEClass, TagInterface.class, "TagInterface", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTagInterface_TagName(), ecorePackage.getEString(), "tagName", null, 0, 1, TagInterface.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTagInterface_Text(), ecorePackage.getEString(), "text", null, 0, 1, TagInterface.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTagInterface_Tail(), ecorePackage.getEString(), "tail", null, 0, 1, TagInterface.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getTagInterface__GetParentTag(), this.getTagInterface(), "getParentTag", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getTagInterface__GetHtmlAttributes(), ecorePackage.getEAttribute(), "getHtmlAttributes", 0, -1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getTagInterface__GetChildTags(), this.getTagInterface(), "getChildTags", 0, -1, IS_UNIQUE, IS_ORDERED);

		initEClass(commonTagParentInterfaceEClass, CommonTagParentInterface.class, "CommonTagParentInterface", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCommonTagParentInterface_Children(), this.getCommonTagInterface(), this.getCommonTagInterface_Parent(), "children", null, 0, -1, CommonTagParentInterface.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(commonTagInterfaceEClass, CommonTagInterface.class, "CommonTagInterface", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCommonTagInterface_Parent(), this.getCommonTagParentInterface(), this.getCommonTagParentInterface_Children(), "parent", null, 0, 1, CommonTagInterface.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(htmlTagEClass, HtmlTag.class, "HtmlTag", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getHtmlTag_Body(), this.getBodyTag(), this.getBodyTag_Parent(), "body", null, 0, 1, HtmlTag.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getHtmlTag_Head(), this.getHeadTag(), this.getHeadTag_Parent(), "head", null, 0, 1, HtmlTag.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(headTagEClass, HeadTag.class, "HeadTag", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getHeadTag_Parent(), this.getHtmlTag(), this.getHtmlTag_Head(), "parent", null, 0, 1, HeadTag.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getHeadTag_Title(), this.getTitleTag(), this.getTitleTag_Parent(), "title", null, 0, 1, HeadTag.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(titleTagEClass, TitleTag.class, "TitleTag", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTitleTag_Parent(), this.getHeadTag(), this.getHeadTag_Title(), "parent", null, 0, 1, TitleTag.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(bodyTagEClass, BodyTag.class, "BodyTag", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBodyTag_Parent(), this.getHtmlTag(), this.getHtmlTag_Body(), "parent", null, 0, 1, BodyTag.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(standardTagEClass, StandardTag.class, "StandardTag", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getStandardTag_Type(), this.getStandardTagType(), "type", null, 1, 1, StandardTag.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(aTagEClass, ATag.class, "ATag", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getATag_Href(), ecorePackage.getEString(), "href", null, 0, 1, ATag.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getATag_Title(), ecorePackage.getEString(), "title", null, 0, 1, ATag.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(tableTagEClass, TableTag.class, "TableTag", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTableTag_Thead(), this.getTsectionTag(), null, "thead", null, 0, 1, TableTag.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getTableTag_Tbody(), this.getTsectionTag(), null, "tbody", null, 0, 1, TableTag.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getTableTag_Tsections(), this.getTsectionTag(), this.getTsectionTag_Parent(), "tsections", null, 0, 2, TableTag.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(tsectionTagEClass, TsectionTag.class, "TsectionTag", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTsectionTag_Parent(), this.getTableTag(), this.getTableTag_Tsections(), "parent", null, 0, 1, TsectionTag.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTsectionTag_Thead(), ecorePackage.getEBoolean(), "thead", "false", 1, 1, TsectionTag.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTsectionTag_Rows(), this.getTrTag(), this.getTrTag_Parent(), "rows", null, 0, -1, TsectionTag.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(trTagEClass, TrTag.class, "TrTag", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTrTag_Parent(), this.getTsectionTag(), this.getTsectionTag_Rows(), "parent", null, 0, 1, TrTag.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTrTag_Cells(), this.getTCellTag(), this.getTCellTag_Parent(), "cells", null, 0, -1, TrTag.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(tCellTagEClass, TCellTag.class, "TCellTag", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTCellTag_Parent(), this.getTrTag(), this.getTrTag_Cells(), "parent", null, 0, 1, TCellTag.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTCellTag_Th(), ecorePackage.getEBoolean(), "th", null, 0, 1, TCellTag.class, !IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(standardTagTypeEEnum, StandardTagType.class, "StandardTagType");
		addEEnumLiteral(standardTagTypeEEnum, StandardTagType.DIV);
		addEEnumLiteral(standardTagTypeEEnum, StandardTagType.P);
		addEEnumLiteral(standardTagTypeEEnum, StandardTagType.H1);
		addEEnumLiteral(standardTagTypeEEnum, StandardTagType.H2);
		addEEnumLiteral(standardTagTypeEEnum, StandardTagType.H3);

		// Create resource
		createResource(eNS_URI);
	}

} //Html5PackageImpl
