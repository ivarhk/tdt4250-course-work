/**
 */
package html5.util;

import html5.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see html5.Html5Package
 * @generated
 */
public class Html5Switch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static Html5Package modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Html5Switch() {
		if (modelPackage == null) {
			modelPackage = Html5Package.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case Html5Package.TAG_INTERFACE: {
				TagInterface tagInterface = (TagInterface)theEObject;
				T result = caseTagInterface(tagInterface);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Html5Package.COMMON_TAG_PARENT_INTERFACE: {
				CommonTagParentInterface commonTagParentInterface = (CommonTagParentInterface)theEObject;
				T result = caseCommonTagParentInterface(commonTagParentInterface);
				if (result == null) result = caseTagInterface(commonTagParentInterface);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Html5Package.COMMON_TAG_INTERFACE: {
				CommonTagInterface commonTagInterface = (CommonTagInterface)theEObject;
				T result = caseCommonTagInterface(commonTagInterface);
				if (result == null) result = caseTagInterface(commonTagInterface);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Html5Package.HTML_TAG: {
				HtmlTag htmlTag = (HtmlTag)theEObject;
				T result = caseHtmlTag(htmlTag);
				if (result == null) result = caseTagInterface(htmlTag);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Html5Package.HEAD_TAG: {
				HeadTag headTag = (HeadTag)theEObject;
				T result = caseHeadTag(headTag);
				if (result == null) result = caseTagInterface(headTag);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Html5Package.TITLE_TAG: {
				TitleTag titleTag = (TitleTag)theEObject;
				T result = caseTitleTag(titleTag);
				if (result == null) result = caseTagInterface(titleTag);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Html5Package.BODY_TAG: {
				BodyTag bodyTag = (BodyTag)theEObject;
				T result = caseBodyTag(bodyTag);
				if (result == null) result = caseCommonTagParentInterface(bodyTag);
				if (result == null) result = caseTagInterface(bodyTag);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Html5Package.STANDARD_TAG: {
				StandardTag standardTag = (StandardTag)theEObject;
				T result = caseStandardTag(standardTag);
				if (result == null) result = caseCommonTagParentInterface(standardTag);
				if (result == null) result = caseCommonTagInterface(standardTag);
				if (result == null) result = caseTagInterface(standardTag);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Html5Package.ATAG: {
				ATag aTag = (ATag)theEObject;
				T result = caseATag(aTag);
				if (result == null) result = caseCommonTagParentInterface(aTag);
				if (result == null) result = caseCommonTagInterface(aTag);
				if (result == null) result = caseTagInterface(aTag);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Html5Package.TABLE_TAG: {
				TableTag tableTag = (TableTag)theEObject;
				T result = caseTableTag(tableTag);
				if (result == null) result = caseCommonTagInterface(tableTag);
				if (result == null) result = caseTagInterface(tableTag);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Html5Package.TSECTION_TAG: {
				TsectionTag tsectionTag = (TsectionTag)theEObject;
				T result = caseTsectionTag(tsectionTag);
				if (result == null) result = caseTagInterface(tsectionTag);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Html5Package.TR_TAG: {
				TrTag trTag = (TrTag)theEObject;
				T result = caseTrTag(trTag);
				if (result == null) result = caseTagInterface(trTag);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Html5Package.TCELL_TAG: {
				TCellTag tCellTag = (TCellTag)theEObject;
				T result = caseTCellTag(tCellTag);
				if (result == null) result = caseCommonTagParentInterface(tCellTag);
				if (result == null) result = caseTagInterface(tCellTag);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Tag Interface</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Tag Interface</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTagInterface(TagInterface object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Common Tag Parent Interface</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Common Tag Parent Interface</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCommonTagParentInterface(CommonTagParentInterface object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Common Tag Interface</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Common Tag Interface</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCommonTagInterface(CommonTagInterface object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Html Tag</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Html Tag</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHtmlTag(HtmlTag object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Head Tag</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Head Tag</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHeadTag(HeadTag object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Title Tag</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Title Tag</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTitleTag(TitleTag object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Body Tag</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Body Tag</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBodyTag(BodyTag object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Standard Tag</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Standard Tag</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStandardTag(StandardTag object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ATag</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ATag</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseATag(ATag object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Table Tag</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Table Tag</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTableTag(TableTag object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Tsection Tag</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Tsection Tag</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTsectionTag(TsectionTag object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Tr Tag</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Tr Tag</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTrTag(TrTag object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>TCell Tag</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>TCell Tag</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTCellTag(TCellTag object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //Html5Switch
