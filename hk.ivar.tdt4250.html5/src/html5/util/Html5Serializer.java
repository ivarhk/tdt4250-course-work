package html5.util;

import org.eclipse.emf.ecore.EAttribute;

import html5.HtmlTag;
import html5.TagInterface;

public class Html5Serializer {
	
	static final String DOCTYPE_LINE = "<!DOCTYPE html>";
	static final String SINGLE_INDENT = "  ";
	
	public static String serialize(HtmlTag root) {
		StringBuilder sb = new StringBuilder();
		sb.append(DOCTYPE_LINE);
		serializeRecursively(sb, root, -1);
		return sb.toString();
	}
	
	private static void serializeRecursively(StringBuilder sb, TagInterface tag, int indent) {
		newLine(sb, indent);
		sb.append("<");
		sb.append(tag.getTagName());
		serializeAttributes(sb, tag, indent);
		if (isBlank(tag.getText()) && tag.getChildTags().isEmpty()) {
			sb.append("/>");
		} else {
			sb.append(">");
			if (!isBlank(tag.getText())) {
				newLine(sb, indent + 1);
				sb.append(tag.getText());
			}
			for (TagInterface child : tag.getChildTags()) serializeRecursively(sb, child, indent + 1);
			newLine(sb, indent);
			sb.append("</");
			sb.append(tag.getTagName());
			sb.append(">");
		}
		if (!isBlank(tag.getTail())) {
			newLine(sb, indent);
			sb.append(tag.getTail());
		}
	}
	
	private static void serializeAttributes(StringBuilder sb, TagInterface tag, int indent) {
		for (EAttribute attr : tag.getHtmlAttributes()) {
			newLine(sb, indent + 2);
			sb.append(attr.getName());
			sb.append("=\"");
			sb.append(tag.eGet(attr));
			sb.append("\"");
		}
	}
	
	private static void newLine(StringBuilder sb, int indent) {
		sb.append("\n");
		for (int i = 0; i < indent; i++) sb.append(SINGLE_INDENT);
	}
	
	private static boolean isBlank(String someString) {
		return someString == null || someString.isBlank();
	}

}
