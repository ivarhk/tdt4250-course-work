/**
 */
package html5.util;

import html5.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see html5.Html5Package
 * @generated
 */
public class Html5AdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static Html5Package modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Html5AdapterFactory() {
		if (modelPackage == null) {
			modelPackage = Html5Package.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Html5Switch<Adapter> modelSwitch =
		new Html5Switch<Adapter>() {
			@Override
			public Adapter caseTagInterface(TagInterface object) {
				return createTagInterfaceAdapter();
			}
			@Override
			public Adapter caseCommonTagParentInterface(CommonTagParentInterface object) {
				return createCommonTagParentInterfaceAdapter();
			}
			@Override
			public Adapter caseCommonTagInterface(CommonTagInterface object) {
				return createCommonTagInterfaceAdapter();
			}
			@Override
			public Adapter caseHtmlTag(HtmlTag object) {
				return createHtmlTagAdapter();
			}
			@Override
			public Adapter caseHeadTag(HeadTag object) {
				return createHeadTagAdapter();
			}
			@Override
			public Adapter caseTitleTag(TitleTag object) {
				return createTitleTagAdapter();
			}
			@Override
			public Adapter caseBodyTag(BodyTag object) {
				return createBodyTagAdapter();
			}
			@Override
			public Adapter caseStandardTag(StandardTag object) {
				return createStandardTagAdapter();
			}
			@Override
			public Adapter caseATag(ATag object) {
				return createATagAdapter();
			}
			@Override
			public Adapter caseTableTag(TableTag object) {
				return createTableTagAdapter();
			}
			@Override
			public Adapter caseTsectionTag(TsectionTag object) {
				return createTsectionTagAdapter();
			}
			@Override
			public Adapter caseTrTag(TrTag object) {
				return createTrTagAdapter();
			}
			@Override
			public Adapter caseTCellTag(TCellTag object) {
				return createTCellTagAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link html5.TagInterface <em>Tag Interface</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see html5.TagInterface
	 * @generated
	 */
	public Adapter createTagInterfaceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link html5.CommonTagParentInterface <em>Common Tag Parent Interface</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see html5.CommonTagParentInterface
	 * @generated
	 */
	public Adapter createCommonTagParentInterfaceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link html5.CommonTagInterface <em>Common Tag Interface</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see html5.CommonTagInterface
	 * @generated
	 */
	public Adapter createCommonTagInterfaceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link html5.HtmlTag <em>Html Tag</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see html5.HtmlTag
	 * @generated
	 */
	public Adapter createHtmlTagAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link html5.HeadTag <em>Head Tag</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see html5.HeadTag
	 * @generated
	 */
	public Adapter createHeadTagAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link html5.TitleTag <em>Title Tag</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see html5.TitleTag
	 * @generated
	 */
	public Adapter createTitleTagAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link html5.BodyTag <em>Body Tag</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see html5.BodyTag
	 * @generated
	 */
	public Adapter createBodyTagAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link html5.StandardTag <em>Standard Tag</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see html5.StandardTag
	 * @generated
	 */
	public Adapter createStandardTagAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link html5.ATag <em>ATag</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see html5.ATag
	 * @generated
	 */
	public Adapter createATagAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link html5.TableTag <em>Table Tag</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see html5.TableTag
	 * @generated
	 */
	public Adapter createTableTagAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link html5.TsectionTag <em>Tsection Tag</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see html5.TsectionTag
	 * @generated
	 */
	public Adapter createTsectionTagAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link html5.TrTag <em>Tr Tag</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see html5.TrTag
	 * @generated
	 */
	public Adapter createTrTagAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link html5.TCellTag <em>TCell Tag</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see html5.TCellTag
	 * @generated
	 */
	public Adapter createTCellTagAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //Html5AdapterFactory
