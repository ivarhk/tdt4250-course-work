/**
 */
package html5;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Tr Tag</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link html5.TrTag#getParent <em>Parent</em>}</li>
 *   <li>{@link html5.TrTag#getCells <em>Cells</em>}</li>
 * </ul>
 *
 * @see html5.Html5Package#getTrTag()
 * @model
 * @generated
 */
public interface TrTag extends TagInterface {
	/**
	 * Returns the value of the '<em><b>Parent</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link html5.TsectionTag#getRows <em>Rows</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent</em>' container reference.
	 * @see #setParent(TsectionTag)
	 * @see html5.Html5Package#getTrTag_Parent()
	 * @see html5.TsectionTag#getRows
	 * @model opposite="rows" transient="false"
	 * @generated
	 */
	TsectionTag getParent();

	/**
	 * Sets the value of the '{@link html5.TrTag#getParent <em>Parent</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent</em>' container reference.
	 * @see #getParent()
	 * @generated
	 */
	void setParent(TsectionTag value);

	/**
	 * Returns the value of the '<em><b>Cells</b></em>' containment reference list.
	 * The list contents are of type {@link html5.TCellTag}.
	 * It is bidirectional and its opposite is '{@link html5.TCellTag#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cells</em>' containment reference list.
	 * @see html5.Html5Package#getTrTag_Cells()
	 * @see html5.TCellTag#getParent
	 * @model opposite="parent" containment="true"
	 * @generated
	 */
	EList<TCellTag> getCells();

} // TrTag
