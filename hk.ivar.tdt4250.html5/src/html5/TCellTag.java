/**
 */
package html5;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TCell Tag</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link html5.TCellTag#getParent <em>Parent</em>}</li>
 *   <li>{@link html5.TCellTag#isTh <em>Th</em>}</li>
 * </ul>
 *
 * @see html5.Html5Package#getTCellTag()
 * @model
 * @generated
 */
public interface TCellTag extends CommonTagParentInterface {
	/**
	 * Returns the value of the '<em><b>Parent</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link html5.TrTag#getCells <em>Cells</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent</em>' container reference.
	 * @see #setParent(TrTag)
	 * @see html5.Html5Package#getTCellTag_Parent()
	 * @see html5.TrTag#getCells
	 * @model opposite="cells" transient="false"
	 * @generated
	 */
	TrTag getParent();

	/**
	 * Sets the value of the '{@link html5.TCellTag#getParent <em>Parent</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent</em>' container reference.
	 * @see #getParent()
	 * @generated
	 */
	void setParent(TrTag value);

	/**
	 * Returns the value of the '<em><b>Th</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Th</em>' attribute.
	 * @see html5.Html5Package#getTCellTag_Th()
	 * @model changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	boolean isTh();

} // TCellTag
