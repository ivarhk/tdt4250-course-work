/**
 */
package html5;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see html5.Html5Factory
 * @model kind="package"
 * @generated
 */
public interface Html5Package extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "html5";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "platform:/plugin/hk.ivar.tdt4250.html5/model/html5.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "html5";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Html5Package eINSTANCE = html5.impl.Html5PackageImpl.init();

	/**
	 * The meta object id for the '{@link html5.TagInterface <em>Tag Interface</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see html5.TagInterface
	 * @see html5.impl.Html5PackageImpl#getTagInterface()
	 * @generated
	 */
	int TAG_INTERFACE = 0;

	/**
	 * The feature id for the '<em><b>Tag Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAG_INTERFACE__TAG_NAME = 0;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAG_INTERFACE__TEXT = 1;

	/**
	 * The feature id for the '<em><b>Tail</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAG_INTERFACE__TAIL = 2;

	/**
	 * The number of structural features of the '<em>Tag Interface</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAG_INTERFACE_FEATURE_COUNT = 3;

	/**
	 * The operation id for the '<em>Get Parent Tag</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAG_INTERFACE___GET_PARENT_TAG = 0;

	/**
	 * The operation id for the '<em>Get Html Attributes</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAG_INTERFACE___GET_HTML_ATTRIBUTES = 1;

	/**
	 * The operation id for the '<em>Get Child Tags</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAG_INTERFACE___GET_CHILD_TAGS = 2;

	/**
	 * The number of operations of the '<em>Tag Interface</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAG_INTERFACE_OPERATION_COUNT = 3;

	/**
	 * The meta object id for the '{@link html5.CommonTagParentInterface <em>Common Tag Parent Interface</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see html5.CommonTagParentInterface
	 * @see html5.impl.Html5PackageImpl#getCommonTagParentInterface()
	 * @generated
	 */
	int COMMON_TAG_PARENT_INTERFACE = 1;

	/**
	 * The feature id for the '<em><b>Tag Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMON_TAG_PARENT_INTERFACE__TAG_NAME = TAG_INTERFACE__TAG_NAME;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMON_TAG_PARENT_INTERFACE__TEXT = TAG_INTERFACE__TEXT;

	/**
	 * The feature id for the '<em><b>Tail</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMON_TAG_PARENT_INTERFACE__TAIL = TAG_INTERFACE__TAIL;

	/**
	 * The feature id for the '<em><b>Children</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMON_TAG_PARENT_INTERFACE__CHILDREN = TAG_INTERFACE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Common Tag Parent Interface</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMON_TAG_PARENT_INTERFACE_FEATURE_COUNT = TAG_INTERFACE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Parent Tag</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMON_TAG_PARENT_INTERFACE___GET_PARENT_TAG = TAG_INTERFACE___GET_PARENT_TAG;

	/**
	 * The operation id for the '<em>Get Html Attributes</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMON_TAG_PARENT_INTERFACE___GET_HTML_ATTRIBUTES = TAG_INTERFACE___GET_HTML_ATTRIBUTES;

	/**
	 * The operation id for the '<em>Get Child Tags</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMON_TAG_PARENT_INTERFACE___GET_CHILD_TAGS = TAG_INTERFACE___GET_CHILD_TAGS;

	/**
	 * The number of operations of the '<em>Common Tag Parent Interface</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMON_TAG_PARENT_INTERFACE_OPERATION_COUNT = TAG_INTERFACE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link html5.CommonTagInterface <em>Common Tag Interface</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see html5.CommonTagInterface
	 * @see html5.impl.Html5PackageImpl#getCommonTagInterface()
	 * @generated
	 */
	int COMMON_TAG_INTERFACE = 2;

	/**
	 * The feature id for the '<em><b>Tag Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMON_TAG_INTERFACE__TAG_NAME = TAG_INTERFACE__TAG_NAME;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMON_TAG_INTERFACE__TEXT = TAG_INTERFACE__TEXT;

	/**
	 * The feature id for the '<em><b>Tail</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMON_TAG_INTERFACE__TAIL = TAG_INTERFACE__TAIL;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMON_TAG_INTERFACE__PARENT = TAG_INTERFACE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Common Tag Interface</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMON_TAG_INTERFACE_FEATURE_COUNT = TAG_INTERFACE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Parent Tag</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMON_TAG_INTERFACE___GET_PARENT_TAG = TAG_INTERFACE___GET_PARENT_TAG;

	/**
	 * The operation id for the '<em>Get Html Attributes</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMON_TAG_INTERFACE___GET_HTML_ATTRIBUTES = TAG_INTERFACE___GET_HTML_ATTRIBUTES;

	/**
	 * The operation id for the '<em>Get Child Tags</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMON_TAG_INTERFACE___GET_CHILD_TAGS = TAG_INTERFACE___GET_CHILD_TAGS;

	/**
	 * The number of operations of the '<em>Common Tag Interface</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMON_TAG_INTERFACE_OPERATION_COUNT = TAG_INTERFACE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link html5.impl.HtmlTagImpl <em>Html Tag</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see html5.impl.HtmlTagImpl
	 * @see html5.impl.Html5PackageImpl#getHtmlTag()
	 * @generated
	 */
	int HTML_TAG = 3;

	/**
	 * The feature id for the '<em><b>Tag Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HTML_TAG__TAG_NAME = TAG_INTERFACE__TAG_NAME;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HTML_TAG__TEXT = TAG_INTERFACE__TEXT;

	/**
	 * The feature id for the '<em><b>Tail</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HTML_TAG__TAIL = TAG_INTERFACE__TAIL;

	/**
	 * The feature id for the '<em><b>Body</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HTML_TAG__BODY = TAG_INTERFACE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Head</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HTML_TAG__HEAD = TAG_INTERFACE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Html Tag</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HTML_TAG_FEATURE_COUNT = TAG_INTERFACE_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Get Parent Tag</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HTML_TAG___GET_PARENT_TAG = TAG_INTERFACE___GET_PARENT_TAG;

	/**
	 * The operation id for the '<em>Get Html Attributes</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HTML_TAG___GET_HTML_ATTRIBUTES = TAG_INTERFACE___GET_HTML_ATTRIBUTES;

	/**
	 * The operation id for the '<em>Get Child Tags</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HTML_TAG___GET_CHILD_TAGS = TAG_INTERFACE___GET_CHILD_TAGS;

	/**
	 * The number of operations of the '<em>Html Tag</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HTML_TAG_OPERATION_COUNT = TAG_INTERFACE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link html5.impl.HeadTagImpl <em>Head Tag</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see html5.impl.HeadTagImpl
	 * @see html5.impl.Html5PackageImpl#getHeadTag()
	 * @generated
	 */
	int HEAD_TAG = 4;

	/**
	 * The feature id for the '<em><b>Tag Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HEAD_TAG__TAG_NAME = TAG_INTERFACE__TAG_NAME;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HEAD_TAG__TEXT = TAG_INTERFACE__TEXT;

	/**
	 * The feature id for the '<em><b>Tail</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HEAD_TAG__TAIL = TAG_INTERFACE__TAIL;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HEAD_TAG__PARENT = TAG_INTERFACE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Title</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HEAD_TAG__TITLE = TAG_INTERFACE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Head Tag</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HEAD_TAG_FEATURE_COUNT = TAG_INTERFACE_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Get Parent Tag</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HEAD_TAG___GET_PARENT_TAG = TAG_INTERFACE___GET_PARENT_TAG;

	/**
	 * The operation id for the '<em>Get Html Attributes</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HEAD_TAG___GET_HTML_ATTRIBUTES = TAG_INTERFACE___GET_HTML_ATTRIBUTES;

	/**
	 * The operation id for the '<em>Get Child Tags</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HEAD_TAG___GET_CHILD_TAGS = TAG_INTERFACE___GET_CHILD_TAGS;

	/**
	 * The number of operations of the '<em>Head Tag</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HEAD_TAG_OPERATION_COUNT = TAG_INTERFACE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link html5.impl.TitleTagImpl <em>Title Tag</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see html5.impl.TitleTagImpl
	 * @see html5.impl.Html5PackageImpl#getTitleTag()
	 * @generated
	 */
	int TITLE_TAG = 5;

	/**
	 * The feature id for the '<em><b>Tag Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TITLE_TAG__TAG_NAME = TAG_INTERFACE__TAG_NAME;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TITLE_TAG__TEXT = TAG_INTERFACE__TEXT;

	/**
	 * The feature id for the '<em><b>Tail</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TITLE_TAG__TAIL = TAG_INTERFACE__TAIL;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TITLE_TAG__PARENT = TAG_INTERFACE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Title Tag</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TITLE_TAG_FEATURE_COUNT = TAG_INTERFACE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Parent Tag</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TITLE_TAG___GET_PARENT_TAG = TAG_INTERFACE___GET_PARENT_TAG;

	/**
	 * The operation id for the '<em>Get Html Attributes</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TITLE_TAG___GET_HTML_ATTRIBUTES = TAG_INTERFACE___GET_HTML_ATTRIBUTES;

	/**
	 * The operation id for the '<em>Get Child Tags</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TITLE_TAG___GET_CHILD_TAGS = TAG_INTERFACE___GET_CHILD_TAGS;

	/**
	 * The number of operations of the '<em>Title Tag</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TITLE_TAG_OPERATION_COUNT = TAG_INTERFACE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link html5.impl.BodyTagImpl <em>Body Tag</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see html5.impl.BodyTagImpl
	 * @see html5.impl.Html5PackageImpl#getBodyTag()
	 * @generated
	 */
	int BODY_TAG = 6;

	/**
	 * The feature id for the '<em><b>Tag Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BODY_TAG__TAG_NAME = COMMON_TAG_PARENT_INTERFACE__TAG_NAME;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BODY_TAG__TEXT = COMMON_TAG_PARENT_INTERFACE__TEXT;

	/**
	 * The feature id for the '<em><b>Tail</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BODY_TAG__TAIL = COMMON_TAG_PARENT_INTERFACE__TAIL;

	/**
	 * The feature id for the '<em><b>Children</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BODY_TAG__CHILDREN = COMMON_TAG_PARENT_INTERFACE__CHILDREN;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BODY_TAG__PARENT = COMMON_TAG_PARENT_INTERFACE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Body Tag</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BODY_TAG_FEATURE_COUNT = COMMON_TAG_PARENT_INTERFACE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Parent Tag</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BODY_TAG___GET_PARENT_TAG = COMMON_TAG_PARENT_INTERFACE___GET_PARENT_TAG;

	/**
	 * The operation id for the '<em>Get Html Attributes</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BODY_TAG___GET_HTML_ATTRIBUTES = COMMON_TAG_PARENT_INTERFACE___GET_HTML_ATTRIBUTES;

	/**
	 * The operation id for the '<em>Get Child Tags</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BODY_TAG___GET_CHILD_TAGS = COMMON_TAG_PARENT_INTERFACE___GET_CHILD_TAGS;

	/**
	 * The number of operations of the '<em>Body Tag</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BODY_TAG_OPERATION_COUNT = COMMON_TAG_PARENT_INTERFACE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link html5.impl.StandardTagImpl <em>Standard Tag</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see html5.impl.StandardTagImpl
	 * @see html5.impl.Html5PackageImpl#getStandardTag()
	 * @generated
	 */
	int STANDARD_TAG = 7;

	/**
	 * The feature id for the '<em><b>Tag Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STANDARD_TAG__TAG_NAME = COMMON_TAG_PARENT_INTERFACE__TAG_NAME;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STANDARD_TAG__TEXT = COMMON_TAG_PARENT_INTERFACE__TEXT;

	/**
	 * The feature id for the '<em><b>Tail</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STANDARD_TAG__TAIL = COMMON_TAG_PARENT_INTERFACE__TAIL;

	/**
	 * The feature id for the '<em><b>Children</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STANDARD_TAG__CHILDREN = COMMON_TAG_PARENT_INTERFACE__CHILDREN;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STANDARD_TAG__PARENT = COMMON_TAG_PARENT_INTERFACE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STANDARD_TAG__TYPE = COMMON_TAG_PARENT_INTERFACE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Standard Tag</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STANDARD_TAG_FEATURE_COUNT = COMMON_TAG_PARENT_INTERFACE_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Get Parent Tag</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STANDARD_TAG___GET_PARENT_TAG = COMMON_TAG_PARENT_INTERFACE___GET_PARENT_TAG;

	/**
	 * The operation id for the '<em>Get Html Attributes</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STANDARD_TAG___GET_HTML_ATTRIBUTES = COMMON_TAG_PARENT_INTERFACE___GET_HTML_ATTRIBUTES;

	/**
	 * The operation id for the '<em>Get Child Tags</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STANDARD_TAG___GET_CHILD_TAGS = COMMON_TAG_PARENT_INTERFACE___GET_CHILD_TAGS;

	/**
	 * The number of operations of the '<em>Standard Tag</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STANDARD_TAG_OPERATION_COUNT = COMMON_TAG_PARENT_INTERFACE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link html5.impl.ATagImpl <em>ATag</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see html5.impl.ATagImpl
	 * @see html5.impl.Html5PackageImpl#getATag()
	 * @generated
	 */
	int ATAG = 8;

	/**
	 * The feature id for the '<em><b>Tag Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATAG__TAG_NAME = COMMON_TAG_PARENT_INTERFACE__TAG_NAME;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATAG__TEXT = COMMON_TAG_PARENT_INTERFACE__TEXT;

	/**
	 * The feature id for the '<em><b>Tail</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATAG__TAIL = COMMON_TAG_PARENT_INTERFACE__TAIL;

	/**
	 * The feature id for the '<em><b>Children</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATAG__CHILDREN = COMMON_TAG_PARENT_INTERFACE__CHILDREN;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATAG__PARENT = COMMON_TAG_PARENT_INTERFACE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Href</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATAG__HREF = COMMON_TAG_PARENT_INTERFACE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Title</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATAG__TITLE = COMMON_TAG_PARENT_INTERFACE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>ATag</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATAG_FEATURE_COUNT = COMMON_TAG_PARENT_INTERFACE_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Get Parent Tag</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATAG___GET_PARENT_TAG = COMMON_TAG_PARENT_INTERFACE___GET_PARENT_TAG;

	/**
	 * The operation id for the '<em>Get Html Attributes</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATAG___GET_HTML_ATTRIBUTES = COMMON_TAG_PARENT_INTERFACE___GET_HTML_ATTRIBUTES;

	/**
	 * The operation id for the '<em>Get Child Tags</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATAG___GET_CHILD_TAGS = COMMON_TAG_PARENT_INTERFACE___GET_CHILD_TAGS;

	/**
	 * The number of operations of the '<em>ATag</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATAG_OPERATION_COUNT = COMMON_TAG_PARENT_INTERFACE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link html5.impl.TableTagImpl <em>Table Tag</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see html5.impl.TableTagImpl
	 * @see html5.impl.Html5PackageImpl#getTableTag()
	 * @generated
	 */
	int TABLE_TAG = 9;

	/**
	 * The feature id for the '<em><b>Tag Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_TAG__TAG_NAME = COMMON_TAG_INTERFACE__TAG_NAME;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_TAG__TEXT = COMMON_TAG_INTERFACE__TEXT;

	/**
	 * The feature id for the '<em><b>Tail</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_TAG__TAIL = COMMON_TAG_INTERFACE__TAIL;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_TAG__PARENT = COMMON_TAG_INTERFACE__PARENT;

	/**
	 * The feature id for the '<em><b>Thead</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_TAG__THEAD = COMMON_TAG_INTERFACE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Tbody</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_TAG__TBODY = COMMON_TAG_INTERFACE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Tsections</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_TAG__TSECTIONS = COMMON_TAG_INTERFACE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Table Tag</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_TAG_FEATURE_COUNT = COMMON_TAG_INTERFACE_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Get Parent Tag</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_TAG___GET_PARENT_TAG = COMMON_TAG_INTERFACE___GET_PARENT_TAG;

	/**
	 * The operation id for the '<em>Get Html Attributes</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_TAG___GET_HTML_ATTRIBUTES = COMMON_TAG_INTERFACE___GET_HTML_ATTRIBUTES;

	/**
	 * The operation id for the '<em>Get Child Tags</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_TAG___GET_CHILD_TAGS = COMMON_TAG_INTERFACE___GET_CHILD_TAGS;

	/**
	 * The number of operations of the '<em>Table Tag</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_TAG_OPERATION_COUNT = COMMON_TAG_INTERFACE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link html5.impl.TsectionTagImpl <em>Tsection Tag</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see html5.impl.TsectionTagImpl
	 * @see html5.impl.Html5PackageImpl#getTsectionTag()
	 * @generated
	 */
	int TSECTION_TAG = 10;

	/**
	 * The feature id for the '<em><b>Tag Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TSECTION_TAG__TAG_NAME = TAG_INTERFACE__TAG_NAME;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TSECTION_TAG__TEXT = TAG_INTERFACE__TEXT;

	/**
	 * The feature id for the '<em><b>Tail</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TSECTION_TAG__TAIL = TAG_INTERFACE__TAIL;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TSECTION_TAG__PARENT = TAG_INTERFACE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Thead</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TSECTION_TAG__THEAD = TAG_INTERFACE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Rows</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TSECTION_TAG__ROWS = TAG_INTERFACE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Tsection Tag</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TSECTION_TAG_FEATURE_COUNT = TAG_INTERFACE_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Get Parent Tag</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TSECTION_TAG___GET_PARENT_TAG = TAG_INTERFACE___GET_PARENT_TAG;

	/**
	 * The operation id for the '<em>Get Html Attributes</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TSECTION_TAG___GET_HTML_ATTRIBUTES = TAG_INTERFACE___GET_HTML_ATTRIBUTES;

	/**
	 * The operation id for the '<em>Get Child Tags</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TSECTION_TAG___GET_CHILD_TAGS = TAG_INTERFACE___GET_CHILD_TAGS;

	/**
	 * The number of operations of the '<em>Tsection Tag</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TSECTION_TAG_OPERATION_COUNT = TAG_INTERFACE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link html5.impl.TrTagImpl <em>Tr Tag</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see html5.impl.TrTagImpl
	 * @see html5.impl.Html5PackageImpl#getTrTag()
	 * @generated
	 */
	int TR_TAG = 11;

	/**
	 * The feature id for the '<em><b>Tag Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TR_TAG__TAG_NAME = TAG_INTERFACE__TAG_NAME;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TR_TAG__TEXT = TAG_INTERFACE__TEXT;

	/**
	 * The feature id for the '<em><b>Tail</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TR_TAG__TAIL = TAG_INTERFACE__TAIL;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TR_TAG__PARENT = TAG_INTERFACE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Cells</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TR_TAG__CELLS = TAG_INTERFACE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Tr Tag</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TR_TAG_FEATURE_COUNT = TAG_INTERFACE_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Get Parent Tag</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TR_TAG___GET_PARENT_TAG = TAG_INTERFACE___GET_PARENT_TAG;

	/**
	 * The operation id for the '<em>Get Html Attributes</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TR_TAG___GET_HTML_ATTRIBUTES = TAG_INTERFACE___GET_HTML_ATTRIBUTES;

	/**
	 * The operation id for the '<em>Get Child Tags</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TR_TAG___GET_CHILD_TAGS = TAG_INTERFACE___GET_CHILD_TAGS;

	/**
	 * The number of operations of the '<em>Tr Tag</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TR_TAG_OPERATION_COUNT = TAG_INTERFACE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link html5.impl.TCellTagImpl <em>TCell Tag</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see html5.impl.TCellTagImpl
	 * @see html5.impl.Html5PackageImpl#getTCellTag()
	 * @generated
	 */
	int TCELL_TAG = 12;

	/**
	 * The feature id for the '<em><b>Tag Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCELL_TAG__TAG_NAME = COMMON_TAG_PARENT_INTERFACE__TAG_NAME;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCELL_TAG__TEXT = COMMON_TAG_PARENT_INTERFACE__TEXT;

	/**
	 * The feature id for the '<em><b>Tail</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCELL_TAG__TAIL = COMMON_TAG_PARENT_INTERFACE__TAIL;

	/**
	 * The feature id for the '<em><b>Children</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCELL_TAG__CHILDREN = COMMON_TAG_PARENT_INTERFACE__CHILDREN;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCELL_TAG__PARENT = COMMON_TAG_PARENT_INTERFACE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Th</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCELL_TAG__TH = COMMON_TAG_PARENT_INTERFACE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>TCell Tag</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCELL_TAG_FEATURE_COUNT = COMMON_TAG_PARENT_INTERFACE_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Get Parent Tag</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCELL_TAG___GET_PARENT_TAG = COMMON_TAG_PARENT_INTERFACE___GET_PARENT_TAG;

	/**
	 * The operation id for the '<em>Get Html Attributes</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCELL_TAG___GET_HTML_ATTRIBUTES = COMMON_TAG_PARENT_INTERFACE___GET_HTML_ATTRIBUTES;

	/**
	 * The operation id for the '<em>Get Child Tags</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCELL_TAG___GET_CHILD_TAGS = COMMON_TAG_PARENT_INTERFACE___GET_CHILD_TAGS;

	/**
	 * The number of operations of the '<em>TCell Tag</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCELL_TAG_OPERATION_COUNT = COMMON_TAG_PARENT_INTERFACE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link html5.StandardTagType <em>Standard Tag Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see html5.StandardTagType
	 * @see html5.impl.Html5PackageImpl#getStandardTagType()
	 * @generated
	 */
	int STANDARD_TAG_TYPE = 13;


	/**
	 * Returns the meta object for class '{@link html5.TagInterface <em>Tag Interface</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Tag Interface</em>'.
	 * @see html5.TagInterface
	 * @generated
	 */
	EClass getTagInterface();

	/**
	 * Returns the meta object for the attribute '{@link html5.TagInterface#getTagName <em>Tag Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Tag Name</em>'.
	 * @see html5.TagInterface#getTagName()
	 * @see #getTagInterface()
	 * @generated
	 */
	EAttribute getTagInterface_TagName();

	/**
	 * Returns the meta object for the attribute '{@link html5.TagInterface#getText <em>Text</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Text</em>'.
	 * @see html5.TagInterface#getText()
	 * @see #getTagInterface()
	 * @generated
	 */
	EAttribute getTagInterface_Text();

	/**
	 * Returns the meta object for the attribute '{@link html5.TagInterface#getTail <em>Tail</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Tail</em>'.
	 * @see html5.TagInterface#getTail()
	 * @see #getTagInterface()
	 * @generated
	 */
	EAttribute getTagInterface_Tail();

	/**
	 * Returns the meta object for the '{@link html5.TagInterface#getParentTag() <em>Get Parent Tag</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Parent Tag</em>' operation.
	 * @see html5.TagInterface#getParentTag()
	 * @generated
	 */
	EOperation getTagInterface__GetParentTag();

	/**
	 * Returns the meta object for the '{@link html5.TagInterface#getHtmlAttributes() <em>Get Html Attributes</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Html Attributes</em>' operation.
	 * @see html5.TagInterface#getHtmlAttributes()
	 * @generated
	 */
	EOperation getTagInterface__GetHtmlAttributes();

	/**
	 * Returns the meta object for the '{@link html5.TagInterface#getChildTags() <em>Get Child Tags</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Child Tags</em>' operation.
	 * @see html5.TagInterface#getChildTags()
	 * @generated
	 */
	EOperation getTagInterface__GetChildTags();

	/**
	 * Returns the meta object for class '{@link html5.CommonTagParentInterface <em>Common Tag Parent Interface</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Common Tag Parent Interface</em>'.
	 * @see html5.CommonTagParentInterface
	 * @generated
	 */
	EClass getCommonTagParentInterface();

	/**
	 * Returns the meta object for the containment reference list '{@link html5.CommonTagParentInterface#getChildren <em>Children</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Children</em>'.
	 * @see html5.CommonTagParentInterface#getChildren()
	 * @see #getCommonTagParentInterface()
	 * @generated
	 */
	EReference getCommonTagParentInterface_Children();

	/**
	 * Returns the meta object for class '{@link html5.CommonTagInterface <em>Common Tag Interface</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Common Tag Interface</em>'.
	 * @see html5.CommonTagInterface
	 * @generated
	 */
	EClass getCommonTagInterface();

	/**
	 * Returns the meta object for the container reference '{@link html5.CommonTagInterface#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Parent</em>'.
	 * @see html5.CommonTagInterface#getParent()
	 * @see #getCommonTagInterface()
	 * @generated
	 */
	EReference getCommonTagInterface_Parent();

	/**
	 * Returns the meta object for class '{@link html5.HtmlTag <em>Html Tag</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Html Tag</em>'.
	 * @see html5.HtmlTag
	 * @generated
	 */
	EClass getHtmlTag();

	/**
	 * Returns the meta object for the containment reference '{@link html5.HtmlTag#getBody <em>Body</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Body</em>'.
	 * @see html5.HtmlTag#getBody()
	 * @see #getHtmlTag()
	 * @generated
	 */
	EReference getHtmlTag_Body();

	/**
	 * Returns the meta object for the containment reference '{@link html5.HtmlTag#getHead <em>Head</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Head</em>'.
	 * @see html5.HtmlTag#getHead()
	 * @see #getHtmlTag()
	 * @generated
	 */
	EReference getHtmlTag_Head();

	/**
	 * Returns the meta object for class '{@link html5.HeadTag <em>Head Tag</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Head Tag</em>'.
	 * @see html5.HeadTag
	 * @generated
	 */
	EClass getHeadTag();

	/**
	 * Returns the meta object for the container reference '{@link html5.HeadTag#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Parent</em>'.
	 * @see html5.HeadTag#getParent()
	 * @see #getHeadTag()
	 * @generated
	 */
	EReference getHeadTag_Parent();

	/**
	 * Returns the meta object for the containment reference '{@link html5.HeadTag#getTitle <em>Title</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Title</em>'.
	 * @see html5.HeadTag#getTitle()
	 * @see #getHeadTag()
	 * @generated
	 */
	EReference getHeadTag_Title();

	/**
	 * Returns the meta object for class '{@link html5.TitleTag <em>Title Tag</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Title Tag</em>'.
	 * @see html5.TitleTag
	 * @generated
	 */
	EClass getTitleTag();

	/**
	 * Returns the meta object for the container reference '{@link html5.TitleTag#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Parent</em>'.
	 * @see html5.TitleTag#getParent()
	 * @see #getTitleTag()
	 * @generated
	 */
	EReference getTitleTag_Parent();

	/**
	 * Returns the meta object for class '{@link html5.BodyTag <em>Body Tag</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Body Tag</em>'.
	 * @see html5.BodyTag
	 * @generated
	 */
	EClass getBodyTag();

	/**
	 * Returns the meta object for the container reference '{@link html5.BodyTag#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Parent</em>'.
	 * @see html5.BodyTag#getParent()
	 * @see #getBodyTag()
	 * @generated
	 */
	EReference getBodyTag_Parent();

	/**
	 * Returns the meta object for class '{@link html5.StandardTag <em>Standard Tag</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Standard Tag</em>'.
	 * @see html5.StandardTag
	 * @generated
	 */
	EClass getStandardTag();

	/**
	 * Returns the meta object for the attribute '{@link html5.StandardTag#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see html5.StandardTag#getType()
	 * @see #getStandardTag()
	 * @generated
	 */
	EAttribute getStandardTag_Type();

	/**
	 * Returns the meta object for class '{@link html5.ATag <em>ATag</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ATag</em>'.
	 * @see html5.ATag
	 * @generated
	 */
	EClass getATag();

	/**
	 * Returns the meta object for the attribute '{@link html5.ATag#getHref <em>Href</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Href</em>'.
	 * @see html5.ATag#getHref()
	 * @see #getATag()
	 * @generated
	 */
	EAttribute getATag_Href();

	/**
	 * Returns the meta object for the attribute '{@link html5.ATag#getTitle <em>Title</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Title</em>'.
	 * @see html5.ATag#getTitle()
	 * @see #getATag()
	 * @generated
	 */
	EAttribute getATag_Title();

	/**
	 * Returns the meta object for class '{@link html5.TableTag <em>Table Tag</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Table Tag</em>'.
	 * @see html5.TableTag
	 * @generated
	 */
	EClass getTableTag();

	/**
	 * Returns the meta object for the reference '{@link html5.TableTag#getThead <em>Thead</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Thead</em>'.
	 * @see html5.TableTag#getThead()
	 * @see #getTableTag()
	 * @generated
	 */
	EReference getTableTag_Thead();

	/**
	 * Returns the meta object for the reference '{@link html5.TableTag#getTbody <em>Tbody</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Tbody</em>'.
	 * @see html5.TableTag#getTbody()
	 * @see #getTableTag()
	 * @generated
	 */
	EReference getTableTag_Tbody();

	/**
	 * Returns the meta object for the containment reference list '{@link html5.TableTag#getTsections <em>Tsections</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Tsections</em>'.
	 * @see html5.TableTag#getTsections()
	 * @see #getTableTag()
	 * @generated
	 */
	EReference getTableTag_Tsections();

	/**
	 * Returns the meta object for class '{@link html5.TsectionTag <em>Tsection Tag</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Tsection Tag</em>'.
	 * @see html5.TsectionTag
	 * @generated
	 */
	EClass getTsectionTag();

	/**
	 * Returns the meta object for the container reference '{@link html5.TsectionTag#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Parent</em>'.
	 * @see html5.TsectionTag#getParent()
	 * @see #getTsectionTag()
	 * @generated
	 */
	EReference getTsectionTag_Parent();

	/**
	 * Returns the meta object for the attribute '{@link html5.TsectionTag#isThead <em>Thead</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Thead</em>'.
	 * @see html5.TsectionTag#isThead()
	 * @see #getTsectionTag()
	 * @generated
	 */
	EAttribute getTsectionTag_Thead();

	/**
	 * Returns the meta object for the containment reference list '{@link html5.TsectionTag#getRows <em>Rows</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Rows</em>'.
	 * @see html5.TsectionTag#getRows()
	 * @see #getTsectionTag()
	 * @generated
	 */
	EReference getTsectionTag_Rows();

	/**
	 * Returns the meta object for class '{@link html5.TrTag <em>Tr Tag</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Tr Tag</em>'.
	 * @see html5.TrTag
	 * @generated
	 */
	EClass getTrTag();

	/**
	 * Returns the meta object for the container reference '{@link html5.TrTag#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Parent</em>'.
	 * @see html5.TrTag#getParent()
	 * @see #getTrTag()
	 * @generated
	 */
	EReference getTrTag_Parent();

	/**
	 * Returns the meta object for the containment reference list '{@link html5.TrTag#getCells <em>Cells</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Cells</em>'.
	 * @see html5.TrTag#getCells()
	 * @see #getTrTag()
	 * @generated
	 */
	EReference getTrTag_Cells();

	/**
	 * Returns the meta object for class '{@link html5.TCellTag <em>TCell Tag</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>TCell Tag</em>'.
	 * @see html5.TCellTag
	 * @generated
	 */
	EClass getTCellTag();

	/**
	 * Returns the meta object for the container reference '{@link html5.TCellTag#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Parent</em>'.
	 * @see html5.TCellTag#getParent()
	 * @see #getTCellTag()
	 * @generated
	 */
	EReference getTCellTag_Parent();

	/**
	 * Returns the meta object for the attribute '{@link html5.TCellTag#isTh <em>Th</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Th</em>'.
	 * @see html5.TCellTag#isTh()
	 * @see #getTCellTag()
	 * @generated
	 */
	EAttribute getTCellTag_Th();

	/**
	 * Returns the meta object for enum '{@link html5.StandardTagType <em>Standard Tag Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Standard Tag Type</em>'.
	 * @see html5.StandardTagType
	 * @generated
	 */
	EEnum getStandardTagType();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	Html5Factory getHtml5Factory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link html5.TagInterface <em>Tag Interface</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see html5.TagInterface
		 * @see html5.impl.Html5PackageImpl#getTagInterface()
		 * @generated
		 */
		EClass TAG_INTERFACE = eINSTANCE.getTagInterface();

		/**
		 * The meta object literal for the '<em><b>Tag Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TAG_INTERFACE__TAG_NAME = eINSTANCE.getTagInterface_TagName();

		/**
		 * The meta object literal for the '<em><b>Text</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TAG_INTERFACE__TEXT = eINSTANCE.getTagInterface_Text();

		/**
		 * The meta object literal for the '<em><b>Tail</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TAG_INTERFACE__TAIL = eINSTANCE.getTagInterface_Tail();

		/**
		 * The meta object literal for the '<em><b>Get Parent Tag</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation TAG_INTERFACE___GET_PARENT_TAG = eINSTANCE.getTagInterface__GetParentTag();

		/**
		 * The meta object literal for the '<em><b>Get Html Attributes</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation TAG_INTERFACE___GET_HTML_ATTRIBUTES = eINSTANCE.getTagInterface__GetHtmlAttributes();

		/**
		 * The meta object literal for the '<em><b>Get Child Tags</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation TAG_INTERFACE___GET_CHILD_TAGS = eINSTANCE.getTagInterface__GetChildTags();

		/**
		 * The meta object literal for the '{@link html5.CommonTagParentInterface <em>Common Tag Parent Interface</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see html5.CommonTagParentInterface
		 * @see html5.impl.Html5PackageImpl#getCommonTagParentInterface()
		 * @generated
		 */
		EClass COMMON_TAG_PARENT_INTERFACE = eINSTANCE.getCommonTagParentInterface();

		/**
		 * The meta object literal for the '<em><b>Children</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMON_TAG_PARENT_INTERFACE__CHILDREN = eINSTANCE.getCommonTagParentInterface_Children();

		/**
		 * The meta object literal for the '{@link html5.CommonTagInterface <em>Common Tag Interface</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see html5.CommonTagInterface
		 * @see html5.impl.Html5PackageImpl#getCommonTagInterface()
		 * @generated
		 */
		EClass COMMON_TAG_INTERFACE = eINSTANCE.getCommonTagInterface();

		/**
		 * The meta object literal for the '<em><b>Parent</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMON_TAG_INTERFACE__PARENT = eINSTANCE.getCommonTagInterface_Parent();

		/**
		 * The meta object literal for the '{@link html5.impl.HtmlTagImpl <em>Html Tag</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see html5.impl.HtmlTagImpl
		 * @see html5.impl.Html5PackageImpl#getHtmlTag()
		 * @generated
		 */
		EClass HTML_TAG = eINSTANCE.getHtmlTag();

		/**
		 * The meta object literal for the '<em><b>Body</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HTML_TAG__BODY = eINSTANCE.getHtmlTag_Body();

		/**
		 * The meta object literal for the '<em><b>Head</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HTML_TAG__HEAD = eINSTANCE.getHtmlTag_Head();

		/**
		 * The meta object literal for the '{@link html5.impl.HeadTagImpl <em>Head Tag</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see html5.impl.HeadTagImpl
		 * @see html5.impl.Html5PackageImpl#getHeadTag()
		 * @generated
		 */
		EClass HEAD_TAG = eINSTANCE.getHeadTag();

		/**
		 * The meta object literal for the '<em><b>Parent</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HEAD_TAG__PARENT = eINSTANCE.getHeadTag_Parent();

		/**
		 * The meta object literal for the '<em><b>Title</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HEAD_TAG__TITLE = eINSTANCE.getHeadTag_Title();

		/**
		 * The meta object literal for the '{@link html5.impl.TitleTagImpl <em>Title Tag</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see html5.impl.TitleTagImpl
		 * @see html5.impl.Html5PackageImpl#getTitleTag()
		 * @generated
		 */
		EClass TITLE_TAG = eINSTANCE.getTitleTag();

		/**
		 * The meta object literal for the '<em><b>Parent</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TITLE_TAG__PARENT = eINSTANCE.getTitleTag_Parent();

		/**
		 * The meta object literal for the '{@link html5.impl.BodyTagImpl <em>Body Tag</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see html5.impl.BodyTagImpl
		 * @see html5.impl.Html5PackageImpl#getBodyTag()
		 * @generated
		 */
		EClass BODY_TAG = eINSTANCE.getBodyTag();

		/**
		 * The meta object literal for the '<em><b>Parent</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BODY_TAG__PARENT = eINSTANCE.getBodyTag_Parent();

		/**
		 * The meta object literal for the '{@link html5.impl.StandardTagImpl <em>Standard Tag</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see html5.impl.StandardTagImpl
		 * @see html5.impl.Html5PackageImpl#getStandardTag()
		 * @generated
		 */
		EClass STANDARD_TAG = eINSTANCE.getStandardTag();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STANDARD_TAG__TYPE = eINSTANCE.getStandardTag_Type();

		/**
		 * The meta object literal for the '{@link html5.impl.ATagImpl <em>ATag</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see html5.impl.ATagImpl
		 * @see html5.impl.Html5PackageImpl#getATag()
		 * @generated
		 */
		EClass ATAG = eINSTANCE.getATag();

		/**
		 * The meta object literal for the '<em><b>Href</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATAG__HREF = eINSTANCE.getATag_Href();

		/**
		 * The meta object literal for the '<em><b>Title</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATAG__TITLE = eINSTANCE.getATag_Title();

		/**
		 * The meta object literal for the '{@link html5.impl.TableTagImpl <em>Table Tag</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see html5.impl.TableTagImpl
		 * @see html5.impl.Html5PackageImpl#getTableTag()
		 * @generated
		 */
		EClass TABLE_TAG = eINSTANCE.getTableTag();

		/**
		 * The meta object literal for the '<em><b>Thead</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TABLE_TAG__THEAD = eINSTANCE.getTableTag_Thead();

		/**
		 * The meta object literal for the '<em><b>Tbody</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TABLE_TAG__TBODY = eINSTANCE.getTableTag_Tbody();

		/**
		 * The meta object literal for the '<em><b>Tsections</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TABLE_TAG__TSECTIONS = eINSTANCE.getTableTag_Tsections();

		/**
		 * The meta object literal for the '{@link html5.impl.TsectionTagImpl <em>Tsection Tag</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see html5.impl.TsectionTagImpl
		 * @see html5.impl.Html5PackageImpl#getTsectionTag()
		 * @generated
		 */
		EClass TSECTION_TAG = eINSTANCE.getTsectionTag();

		/**
		 * The meta object literal for the '<em><b>Parent</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TSECTION_TAG__PARENT = eINSTANCE.getTsectionTag_Parent();

		/**
		 * The meta object literal for the '<em><b>Thead</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TSECTION_TAG__THEAD = eINSTANCE.getTsectionTag_Thead();

		/**
		 * The meta object literal for the '<em><b>Rows</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TSECTION_TAG__ROWS = eINSTANCE.getTsectionTag_Rows();

		/**
		 * The meta object literal for the '{@link html5.impl.TrTagImpl <em>Tr Tag</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see html5.impl.TrTagImpl
		 * @see html5.impl.Html5PackageImpl#getTrTag()
		 * @generated
		 */
		EClass TR_TAG = eINSTANCE.getTrTag();

		/**
		 * The meta object literal for the '<em><b>Parent</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TR_TAG__PARENT = eINSTANCE.getTrTag_Parent();

		/**
		 * The meta object literal for the '<em><b>Cells</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TR_TAG__CELLS = eINSTANCE.getTrTag_Cells();

		/**
		 * The meta object literal for the '{@link html5.impl.TCellTagImpl <em>TCell Tag</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see html5.impl.TCellTagImpl
		 * @see html5.impl.Html5PackageImpl#getTCellTag()
		 * @generated
		 */
		EClass TCELL_TAG = eINSTANCE.getTCellTag();

		/**
		 * The meta object literal for the '<em><b>Parent</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TCELL_TAG__PARENT = eINSTANCE.getTCellTag_Parent();

		/**
		 * The meta object literal for the '<em><b>Th</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TCELL_TAG__TH = eINSTANCE.getTCellTag_Th();

		/**
		 * The meta object literal for the '{@link html5.StandardTagType <em>Standard Tag Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see html5.StandardTagType
		 * @see html5.impl.Html5PackageImpl#getStandardTagType()
		 * @generated
		 */
		EEnum STANDARD_TAG_TYPE = eINSTANCE.getStandardTagType();

	}

} //Html5Package
