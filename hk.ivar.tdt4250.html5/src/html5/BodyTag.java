/**
 */
package html5;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Body Tag</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link html5.BodyTag#getParent <em>Parent</em>}</li>
 * </ul>
 *
 * @see html5.Html5Package#getBodyTag()
 * @model
 * @generated
 */
public interface BodyTag extends CommonTagParentInterface {
	/**
	 * Returns the value of the '<em><b>Parent</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link html5.HtmlTag#getBody <em>Body</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent</em>' container reference.
	 * @see #setParent(HtmlTag)
	 * @see html5.Html5Package#getBodyTag_Parent()
	 * @see html5.HtmlTag#getBody
	 * @model opposite="body" transient="false"
	 * @generated
	 */
	HtmlTag getParent();

	/**
	 * Sets the value of the '{@link html5.BodyTag#getParent <em>Parent</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent</em>' container reference.
	 * @see #getParent()
	 * @generated
	 */
	void setParent(HtmlTag value);

} // BodyTag
