/**
 */
package html5;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Common Tag Interface</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link html5.CommonTagInterface#getParent <em>Parent</em>}</li>
 * </ul>
 *
 * @see html5.Html5Package#getCommonTagInterface()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface CommonTagInterface extends TagInterface {
	/**
	 * Returns the value of the '<em><b>Parent</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link html5.CommonTagParentInterface#getChildren <em>Children</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent</em>' container reference.
	 * @see #setParent(CommonTagParentInterface)
	 * @see html5.Html5Package#getCommonTagInterface_Parent()
	 * @see html5.CommonTagParentInterface#getChildren
	 * @model opposite="children" transient="false"
	 * @generated
	 */
	CommonTagParentInterface getParent();

	/**
	 * Sets the value of the '{@link html5.CommonTagInterface#getParent <em>Parent</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent</em>' container reference.
	 * @see #getParent()
	 * @generated
	 */
	void setParent(CommonTagParentInterface value);

} // CommonTagInterface
