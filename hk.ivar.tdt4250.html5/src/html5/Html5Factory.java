/**
 */
package html5;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see html5.Html5Package
 * @generated
 */
public interface Html5Factory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Html5Factory eINSTANCE = html5.impl.Html5FactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Html Tag</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Html Tag</em>'.
	 * @generated
	 */
	HtmlTag createHtmlTag();

	/**
	 * Returns a new object of class '<em>Head Tag</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Head Tag</em>'.
	 * @generated
	 */
	HeadTag createHeadTag();

	/**
	 * Returns a new object of class '<em>Title Tag</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Title Tag</em>'.
	 * @generated
	 */
	TitleTag createTitleTag();

	/**
	 * Returns a new object of class '<em>Body Tag</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Body Tag</em>'.
	 * @generated
	 */
	BodyTag createBodyTag();

	/**
	 * Returns a new object of class '<em>Standard Tag</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Standard Tag</em>'.
	 * @generated
	 */
	StandardTag createStandardTag();

	/**
	 * Returns a new object of class '<em>ATag</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ATag</em>'.
	 * @generated
	 */
	ATag createATag();

	/**
	 * Returns a new object of class '<em>Table Tag</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Table Tag</em>'.
	 * @generated
	 */
	TableTag createTableTag();

	/**
	 * Returns a new object of class '<em>Tsection Tag</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Tsection Tag</em>'.
	 * @generated
	 */
	TsectionTag createTsectionTag();

	/**
	 * Returns a new object of class '<em>Tr Tag</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Tr Tag</em>'.
	 * @generated
	 */
	TrTag createTrTag();

	/**
	 * Returns a new object of class '<em>TCell Tag</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>TCell Tag</em>'.
	 * @generated
	 */
	TCellTag createTCellTag();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	Html5Package getHtml5Package();

} //Html5Factory
