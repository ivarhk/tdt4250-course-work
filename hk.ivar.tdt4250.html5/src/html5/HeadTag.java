/**
 */
package html5;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Head Tag</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link html5.HeadTag#getParent <em>Parent</em>}</li>
 *   <li>{@link html5.HeadTag#getTitle <em>Title</em>}</li>
 * </ul>
 *
 * @see html5.Html5Package#getHeadTag()
 * @model
 * @generated
 */
public interface HeadTag extends TagInterface {
	/**
	 * Returns the value of the '<em><b>Parent</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link html5.HtmlTag#getHead <em>Head</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent</em>' container reference.
	 * @see #setParent(HtmlTag)
	 * @see html5.Html5Package#getHeadTag_Parent()
	 * @see html5.HtmlTag#getHead
	 * @model opposite="head" transient="false"
	 * @generated
	 */
	HtmlTag getParent();

	/**
	 * Sets the value of the '{@link html5.HeadTag#getParent <em>Parent</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent</em>' container reference.
	 * @see #getParent()
	 * @generated
	 */
	void setParent(HtmlTag value);

	/**
	 * Returns the value of the '<em><b>Title</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link html5.TitleTag#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Title</em>' containment reference.
	 * @see #setTitle(TitleTag)
	 * @see html5.Html5Package#getHeadTag_Title()
	 * @see html5.TitleTag#getParent
	 * @model opposite="parent" containment="true"
	 * @generated
	 */
	TitleTag getTitle();

	/**
	 * Sets the value of the '{@link html5.HeadTag#getTitle <em>Title</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Title</em>' containment reference.
	 * @see #getTitle()
	 * @generated
	 */
	void setTitle(TitleTag value);

} // HeadTag
