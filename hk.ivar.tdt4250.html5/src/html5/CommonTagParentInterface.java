/**
 */
package html5;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Common Tag Parent Interface</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link html5.CommonTagParentInterface#getChildren <em>Children</em>}</li>
 * </ul>
 *
 * @see html5.Html5Package#getCommonTagParentInterface()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface CommonTagParentInterface extends TagInterface {
	/**
	 * Returns the value of the '<em><b>Children</b></em>' containment reference list.
	 * The list contents are of type {@link html5.CommonTagInterface}.
	 * It is bidirectional and its opposite is '{@link html5.CommonTagInterface#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Children</em>' containment reference list.
	 * @see html5.Html5Package#getCommonTagParentInterface_Children()
	 * @see html5.CommonTagInterface#getParent
	 * @model opposite="parent" containment="true"
	 * @generated
	 */
	EList<CommonTagInterface> getChildren();

} // CommonTagParentInterface
