-- @path case=/hk.ivar.tdt4250.case/model/case.ecore
-- @path html5=/hk.ivar.tdt4250.html5/model/html5.ecore

module case2html5;
create OUT : html5 from IN : case;

helper context case!SpecializationYear def : hasMultipleOptions() : Boolean =
	self.nextYearOptions->size() > 1;

rule Programme {
	from
		s : case!Programme
	to 
		t : html5!HtmlTag (
			head <- head,
			body <- body
		),
		head : html5!HeadTag (
			title <- title
		),
		title : html5!TitleTag (
			text <- 'Study Plan for ' + s.name			
		),
		body : html5!BodyTag (
			children <- h1,
			children <- p,
			children <- thisModule.NextYearOptions(s.yearZero)
		),
		h1 : html5!StandardTag (
			type <- #h1,
			text <- 'Study Plan'
		),
		p : html5!StandardTag (
			type <- #p,
			text <- s.name
		)
}

lazy rule NormalYear {
	from
		s : case!SpecializationYear
	to
		t : html5!StandardTag (
			type <- 'div',
			children <- thisModule.YearContents(s),
			children <- s.nextYearOptions->collect(y |
				if y.hasMultipleOptions() then
					thisModule.OptionsYear(y)
				else
					thisModule.NormalYear(y)
				endif
			)
		)
}

lazy rule OptionsYear {
	from
		s : case!SpecializationYear
	to
		t : html5!StandardTag (
			type <- 'div',
			children <- thisModule.YearContents(s),
			children <- thisModule.NextYearOptions(s)
		)
}

lazy rule YearContents {
	from
		s : case!SpecializationYear
	to
		t : html5!StandardTag (
			type <- 'div',
			children <- header,
			children <- coursesFall,
			children <- coursesSpring
		),
		header : html5!StandardTag (
			type <- #h3,
			text <- s.name + ' (year ' + s.yearNum + ')'
		),
		coursesFall : html5!StandardTag (
			type <- 'div',
			children <- headerFall,
			children <- s.courseGroups->select(cg | cg.semester = #FALL)
		),
		headerFall : html5!StandardTag (
			type <- #p,
			text <- 'Fall'
		),
		coursesSpring : html5!StandardTag (
			type <- 'div',
			children <- headerSpring,
			children <- s.courseGroups->select(cg | cg.semester = #SPRING)
		),
		headerSpring : html5!StandardTag (
			type <- #p,
			text <- 'Spring'
		)
}

lazy rule NextYearOptions {
	from
		s : case!SpecializationYear
	to
		t : html5!StandardTag (
			type <- 'div',
			children <- h2,
			children <- s.nextYearOptions->collect(y |
				if y.hasMultipleOptions() then
					thisModule.OptionsYear(y)
				else
					thisModule.NormalYear(y)
				endif
			)
		),
		h2 : html5!StandardTag (
			type <- #h2,
			text <- s.nextYearOptionsLabel
		)
}

rule CourseGroup {
	from
		s : case!CourseGroup
	to
		t : html5!StandardTag (
			type <- 'div',
			children <- h3,
			children <- table
		),
		h3 : html5!StandardTag (
			type <- #h3,
			text <- s.name
		),
		table : html5!TableTag (
			tsections <- thead,
			tsections <- tbody
		),
		thead : html5!TsectionTag (
			thead <- true,
			rows <- headerRow
		),
		headerRow : html5!TrTag (
			cells <- thCode,
			cells <- thName,
			cells <- thCredits,
			cells <- thStatus
		),
		thCode : html5!TCellTag (
			text <- 'Course Code'
		),
		thName : html5!TCellTag (
			text <- 'Course Name'
		),
		thCredits : html5!TCellTag (
			text <- 'Credits'
		),
		thStatus : html5!TCellTag (
			text <- 'Status'
		),
		tbody : html5!TsectionTag (
			thead <- false,
			rows <- s.courses
		)
}

rule Course {
	from
		s : case!CourseEntry
	to
		t : html5!TrTag (
			cells <- code,
			cells <- name,
			cells <- credits,
			cells <- status
		),
		code : html5!TCellTag (
			text <- s.course.code
		),
		name : html5!TCellTag (
			text <- s.course.courseName
		),
		credits : html5!TCellTag (
			text <- s.course.credits.toString()
		),
		status : html5!TCellTag (
			text <- s.status.toString()
		)
}
