/**
 */
package case_;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Specialization</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link case_.Specialization#getPlan <em>Plan</em>}</li>
 *   <li>{@link case_.Specialization#getOptionYear <em>Option Year</em>}</li>
 *   <li>{@link case_.Specialization#getName <em>Name</em>}</li>
 *   <li>{@link case_.Specialization#getParent <em>Parent</em>}</li>
 *   <li>{@link case_.Specialization#getChildren <em>Children</em>}</li>
 * </ul>
 *
 * @see case_.CasePackage#getSpecialization()
 * @model
 * @generated
 */
public interface Specialization extends EObject {
	/**
	 * Returns the value of the '<em><b>Plan</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link case_.Plan#getSpecializations <em>Specializations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Plan</em>' container reference.
	 * @see #setPlan(Plan)
	 * @see case_.CasePackage#getSpecialization_Plan()
	 * @see case_.Plan#getSpecializations
	 * @model opposite="specializations" transient="false"
	 * @generated
	 */
	Plan getPlan();

	/**
	 * Sets the value of the '{@link case_.Specialization#getPlan <em>Plan</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Plan</em>' container reference.
	 * @see #getPlan()
	 * @generated
	 */
	void setPlan(Plan value);

	/**
	 * Returns the value of the '<em><b>Option Year</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link case_.Year#getSpecializationOptions <em>Specialization Options</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Option Year</em>' reference.
	 * @see #setOptionYear(Year)
	 * @see case_.CasePackage#getSpecialization_OptionYear()
	 * @see case_.Year#getSpecializationOptions
	 * @model opposite="specializationOptions"
	 * @generated
	 */
	Year getOptionYear();

	/**
	 * Sets the value of the '{@link case_.Specialization#getOptionYear <em>Option Year</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Option Year</em>' reference.
	 * @see #getOptionYear()
	 * @generated
	 */
	void setOptionYear(Year value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see case_.CasePackage#getSpecialization_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link case_.Specialization#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Parent</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link case_.Specialization#getChildren <em>Children</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent</em>' reference.
	 * @see #setParent(Specialization)
	 * @see case_.CasePackage#getSpecialization_Parent()
	 * @see case_.Specialization#getChildren
	 * @model opposite="children"
	 * @generated
	 */
	Specialization getParent();

	/**
	 * Sets the value of the '{@link case_.Specialization#getParent <em>Parent</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent</em>' reference.
	 * @see #getParent()
	 * @generated
	 */
	void setParent(Specialization value);

	/**
	 * Returns the value of the '<em><b>Children</b></em>' reference list.
	 * The list contents are of type {@link case_.Specialization}.
	 * It is bidirectional and its opposite is '{@link case_.Specialization#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Children</em>' reference list.
	 * @see case_.CasePackage#getSpecialization_Children()
	 * @see case_.Specialization#getParent
	 * @model opposite="parent"
	 * @generated
	 */
	EList<Specialization> getChildren();

} // Specialization
