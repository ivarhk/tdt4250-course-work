/**
 */
package case_;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Year</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link case_.Year#getPlan <em>Plan</em>}</li>
 *   <li>{@link case_.Year#getSpecializationsLabel <em>Specializations Label</em>}</li>
 *   <li>{@link case_.Year#getSpecializationOptions <em>Specialization Options</em>}</li>
 *   <li>{@link case_.Year#getCourseGroups <em>Course Groups</em>}</li>
 * </ul>
 *
 * @see case_.CasePackage#getYear()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='doesNotRequireMoreThan60Credits'"
 * @generated
 */
public interface Year extends EObject {
	/**
	 * Returns the value of the '<em><b>Plan</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link case_.Plan#getYears <em>Years</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Plan</em>' container reference.
	 * @see #setPlan(Plan)
	 * @see case_.CasePackage#getYear_Plan()
	 * @see case_.Plan#getYears
	 * @model opposite="years" transient="false"
	 * @generated
	 */
	Plan getPlan();

	/**
	 * Sets the value of the '{@link case_.Year#getPlan <em>Plan</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Plan</em>' container reference.
	 * @see #getPlan()
	 * @generated
	 */
	void setPlan(Plan value);

	/**
	 * Returns the value of the '<em><b>Specializations Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Specializations Label</em>' attribute.
	 * @see #setSpecializationsLabel(String)
	 * @see case_.CasePackage#getYear_SpecializationsLabel()
	 * @model
	 * @generated
	 */
	String getSpecializationsLabel();

	/**
	 * Sets the value of the '{@link case_.Year#getSpecializationsLabel <em>Specializations Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Specializations Label</em>' attribute.
	 * @see #getSpecializationsLabel()
	 * @generated
	 */
	void setSpecializationsLabel(String value);

	/**
	 * Returns the value of the '<em><b>Specialization Options</b></em>' reference list.
	 * The list contents are of type {@link case_.Specialization}.
	 * It is bidirectional and its opposite is '{@link case_.Specialization#getOptionYear <em>Option Year</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Specialization Options</em>' reference list.
	 * @see case_.CasePackage#getYear_SpecializationOptions()
	 * @see case_.Specialization#getOptionYear
	 * @model opposite="optionYear"
	 * @generated
	 */
	EList<Specialization> getSpecializationOptions();

	/**
	 * Returns the value of the '<em><b>Course Groups</b></em>' containment reference list.
	 * The list contents are of type {@link case_.CourseGroup}.
	 * It is bidirectional and its opposite is '{@link case_.CourseGroup#getYear <em>Year</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Course Groups</em>' containment reference list.
	 * @see case_.CasePackage#getYear_CourseGroups()
	 * @see case_.CourseGroup#getYear
	 * @model opposite="year" containment="true" required="true"
	 * @generated
	 */
	EList<CourseGroup> getCourseGroups();

} // Year
