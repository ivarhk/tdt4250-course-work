/**
 */
package case_;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Plan</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link case_.Plan#getProgram <em>Program</em>}</li>
 *   <li>{@link case_.Plan#getEnrollmentYear <em>Enrollment Year</em>}</li>
 *   <li>{@link case_.Plan#getYears <em>Years</em>}</li>
 *   <li>{@link case_.Plan#getSpecializations <em>Specializations</em>}</li>
 * </ul>
 *
 * @see case_.CasePackage#getPlan()
 * @model
 * @generated
 */
public interface Plan extends EObject {
	/**
	 * Returns the value of the '<em><b>Program</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link case_.Programme#getPlans <em>Plans</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Program</em>' container reference.
	 * @see #setProgram(Programme)
	 * @see case_.CasePackage#getPlan_Program()
	 * @see case_.Programme#getPlans
	 * @model opposite="plans" transient="false"
	 * @generated
	 */
	Programme getProgram();

	/**
	 * Sets the value of the '{@link case_.Plan#getProgram <em>Program</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Program</em>' container reference.
	 * @see #getProgram()
	 * @generated
	 */
	void setProgram(Programme value);

	/**
	 * Returns the value of the '<em><b>Enrollment Year</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Enrollment Year</em>' attribute.
	 * @see #setEnrollmentYear(short)
	 * @see case_.CasePackage#getPlan_EnrollmentYear()
	 * @model required="true"
	 * @generated
	 */
	short getEnrollmentYear();

	/**
	 * Sets the value of the '{@link case_.Plan#getEnrollmentYear <em>Enrollment Year</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Enrollment Year</em>' attribute.
	 * @see #getEnrollmentYear()
	 * @generated
	 */
	void setEnrollmentYear(short value);

	/**
	 * Returns the value of the '<em><b>Years</b></em>' containment reference list.
	 * The list contents are of type {@link case_.Year}.
	 * It is bidirectional and its opposite is '{@link case_.Year#getPlan <em>Plan</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Years</em>' containment reference list.
	 * @see case_.CasePackage#getPlan_Years()
	 * @see case_.Year#getPlan
	 * @model opposite="plan" containment="true"
	 * @generated
	 */
	EList<Year> getYears();

	/**
	 * Returns the value of the '<em><b>Specializations</b></em>' containment reference list.
	 * The list contents are of type {@link case_.Specialization}.
	 * It is bidirectional and its opposite is '{@link case_.Specialization#getPlan <em>Plan</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Specializations</em>' containment reference list.
	 * @see case_.CasePackage#getPlan_Specializations()
	 * @see case_.Specialization#getPlan
	 * @model opposite="plan" containment="true"
	 * @generated
	 */
	EList<Specialization> getSpecializations();

} // Plan
