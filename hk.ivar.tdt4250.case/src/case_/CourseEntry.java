/**
 */
package case_;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Course Entry</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link case_.CourseEntry#getGroup <em>Group</em>}</li>
 *   <li>{@link case_.CourseEntry#getCourse <em>Course</em>}</li>
 *   <li>{@link case_.CourseEntry#getAvailableTo <em>Available To</em>}</li>
 *   <li>{@link case_.CourseEntry#getSemester <em>Semester</em>}</li>
 * </ul>
 *
 * @see case_.CasePackage#getCourseEntry()
 * @model
 * @generated
 */
public interface CourseEntry extends EObject {
	/**
	 * Returns the value of the '<em><b>Group</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link case_.CourseGroup#getCourses <em>Courses</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group</em>' container reference.
	 * @see #setGroup(CourseGroup)
	 * @see case_.CasePackage#getCourseEntry_Group()
	 * @see case_.CourseGroup#getCourses
	 * @model opposite="courses" transient="false"
	 * @generated
	 */
	CourseGroup getGroup();

	/**
	 * Sets the value of the '{@link case_.CourseEntry#getGroup <em>Group</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Group</em>' container reference.
	 * @see #getGroup()
	 * @generated
	 */
	void setGroup(CourseGroup value);

	/**
	 * Returns the value of the '<em><b>Course</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link case_.Course#getPlans <em>Plans</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Course</em>' reference.
	 * @see #setCourse(Course)
	 * @see case_.CasePackage#getCourseEntry_Course()
	 * @see case_.Course#getPlans
	 * @model opposite="plans" required="true"
	 * @generated
	 */
	Course getCourse();

	/**
	 * Sets the value of the '{@link case_.CourseEntry#getCourse <em>Course</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Course</em>' reference.
	 * @see #getCourse()
	 * @generated
	 */
	void setCourse(Course value);

	/**
	 * Returns the value of the '<em><b>Available To</b></em>' map.
	 * The key is of type {@link case_.CourseStatus},
	 * and the value is of type list of {@link case_.Specialization},
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Available To</em>' map.
	 * @see case_.CasePackage#getCourseEntry_AvailableTo()
	 * @model mapType="case_.CourseStatusToSpecialization&lt;case_.CourseStatus, case_.Specialization&gt;"
	 * @generated
	 */
	EMap<CourseStatus, EList<Specialization>> getAvailableTo();

	/**
	 * Returns the value of the '<em><b>Semester</b></em>' attribute.
	 * The literals are from the enumeration {@link case_.SemesterType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Semester</em>' attribute.
	 * @see case_.SemesterType
	 * @see #setSemester(SemesterType)
	 * @see case_.CasePackage#getCourseEntry_Semester()
	 * @model required="true"
	 * @generated
	 */
	SemesterType getSemester();

	/**
	 * Sets the value of the '{@link case_.CourseEntry#getSemester <em>Semester</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Semester</em>' attribute.
	 * @see case_.SemesterType
	 * @see #getSemester()
	 * @generated
	 */
	void setSemester(SemesterType value);

} // CourseEntry
