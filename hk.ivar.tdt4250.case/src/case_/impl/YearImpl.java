/**
 */
package case_.impl;

import case_.CasePackage;
import case_.CourseGroup;
import case_.Plan;
import case_.Specialization;
import case_.Year;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Year</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link case_.impl.YearImpl#getPlan <em>Plan</em>}</li>
 *   <li>{@link case_.impl.YearImpl#getSpecializationsLabel <em>Specializations Label</em>}</li>
 *   <li>{@link case_.impl.YearImpl#getSpecializationOptions <em>Specialization Options</em>}</li>
 *   <li>{@link case_.impl.YearImpl#getCourseGroups <em>Course Groups</em>}</li>
 * </ul>
 *
 * @generated
 */
public class YearImpl extends MinimalEObjectImpl.Container implements Year {
	/**
	 * The default value of the '{@link #getSpecializationsLabel() <em>Specializations Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpecializationsLabel()
	 * @generated
	 * @ordered
	 */
	protected static final String SPECIALIZATIONS_LABEL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSpecializationsLabel() <em>Specializations Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpecializationsLabel()
	 * @generated
	 * @ordered
	 */
	protected String specializationsLabel = SPECIALIZATIONS_LABEL_EDEFAULT;

	/**
	 * The cached value of the '{@link #getSpecializationOptions() <em>Specialization Options</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpecializationOptions()
	 * @generated
	 * @ordered
	 */
	protected EList<Specialization> specializationOptions;

	/**
	 * The cached value of the '{@link #getCourseGroups() <em>Course Groups</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCourseGroups()
	 * @generated
	 * @ordered
	 */
	protected EList<CourseGroup> courseGroups;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YearImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CasePackage.Literals.YEAR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Plan getPlan() {
		if (eContainerFeatureID() != CasePackage.YEAR__PLAN) return null;
		return (Plan)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPlan(Plan newPlan, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newPlan, CasePackage.YEAR__PLAN, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPlan(Plan newPlan) {
		if (newPlan != eInternalContainer() || (eContainerFeatureID() != CasePackage.YEAR__PLAN && newPlan != null)) {
			if (EcoreUtil.isAncestor(this, newPlan))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newPlan != null)
				msgs = ((InternalEObject)newPlan).eInverseAdd(this, CasePackage.PLAN__YEARS, Plan.class, msgs);
			msgs = basicSetPlan(newPlan, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CasePackage.YEAR__PLAN, newPlan, newPlan));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSpecializationsLabel() {
		return specializationsLabel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSpecializationsLabel(String newSpecializationsLabel) {
		String oldSpecializationsLabel = specializationsLabel;
		specializationsLabel = newSpecializationsLabel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CasePackage.YEAR__SPECIALIZATIONS_LABEL, oldSpecializationsLabel, specializationsLabel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Specialization> getSpecializationOptions() {
		if (specializationOptions == null) {
			specializationOptions = new EObjectWithInverseResolvingEList<Specialization>(Specialization.class, this, CasePackage.YEAR__SPECIALIZATION_OPTIONS, CasePackage.SPECIALIZATION__OPTION_YEAR);
		}
		return specializationOptions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<CourseGroup> getCourseGroups() {
		if (courseGroups == null) {
			courseGroups = new EObjectContainmentWithInverseEList<CourseGroup>(CourseGroup.class, this, CasePackage.YEAR__COURSE_GROUPS, CasePackage.COURSE_GROUP__YEAR);
		}
		return courseGroups;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CasePackage.YEAR__PLAN:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetPlan((Plan)otherEnd, msgs);
			case CasePackage.YEAR__SPECIALIZATION_OPTIONS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getSpecializationOptions()).basicAdd(otherEnd, msgs);
			case CasePackage.YEAR__COURSE_GROUPS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getCourseGroups()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CasePackage.YEAR__PLAN:
				return basicSetPlan(null, msgs);
			case CasePackage.YEAR__SPECIALIZATION_OPTIONS:
				return ((InternalEList<?>)getSpecializationOptions()).basicRemove(otherEnd, msgs);
			case CasePackage.YEAR__COURSE_GROUPS:
				return ((InternalEList<?>)getCourseGroups()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case CasePackage.YEAR__PLAN:
				return eInternalContainer().eInverseRemove(this, CasePackage.PLAN__YEARS, Plan.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CasePackage.YEAR__PLAN:
				return getPlan();
			case CasePackage.YEAR__SPECIALIZATIONS_LABEL:
				return getSpecializationsLabel();
			case CasePackage.YEAR__SPECIALIZATION_OPTIONS:
				return getSpecializationOptions();
			case CasePackage.YEAR__COURSE_GROUPS:
				return getCourseGroups();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CasePackage.YEAR__PLAN:
				setPlan((Plan)newValue);
				return;
			case CasePackage.YEAR__SPECIALIZATIONS_LABEL:
				setSpecializationsLabel((String)newValue);
				return;
			case CasePackage.YEAR__SPECIALIZATION_OPTIONS:
				getSpecializationOptions().clear();
				getSpecializationOptions().addAll((Collection<? extends Specialization>)newValue);
				return;
			case CasePackage.YEAR__COURSE_GROUPS:
				getCourseGroups().clear();
				getCourseGroups().addAll((Collection<? extends CourseGroup>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CasePackage.YEAR__PLAN:
				setPlan((Plan)null);
				return;
			case CasePackage.YEAR__SPECIALIZATIONS_LABEL:
				setSpecializationsLabel(SPECIALIZATIONS_LABEL_EDEFAULT);
				return;
			case CasePackage.YEAR__SPECIALIZATION_OPTIONS:
				getSpecializationOptions().clear();
				return;
			case CasePackage.YEAR__COURSE_GROUPS:
				getCourseGroups().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CasePackage.YEAR__PLAN:
				return getPlan() != null;
			case CasePackage.YEAR__SPECIALIZATIONS_LABEL:
				return SPECIALIZATIONS_LABEL_EDEFAULT == null ? specializationsLabel != null : !SPECIALIZATIONS_LABEL_EDEFAULT.equals(specializationsLabel);
			case CasePackage.YEAR__SPECIALIZATION_OPTIONS:
				return specializationOptions != null && !specializationOptions.isEmpty();
			case CasePackage.YEAR__COURSE_GROUPS:
				return courseGroups != null && !courseGroups.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (specializationsLabel: ");
		result.append(specializationsLabel);
		result.append(')');
		return result.toString();
	}

} //YearImpl
