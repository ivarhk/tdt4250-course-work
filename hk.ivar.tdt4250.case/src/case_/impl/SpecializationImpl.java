/**
 */
package case_.impl;

import case_.CasePackage;
import case_.Plan;
import case_.Specialization;
import case_.Year;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Specialization</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link case_.impl.SpecializationImpl#getPlan <em>Plan</em>}</li>
 *   <li>{@link case_.impl.SpecializationImpl#getOptionYear <em>Option Year</em>}</li>
 *   <li>{@link case_.impl.SpecializationImpl#getName <em>Name</em>}</li>
 *   <li>{@link case_.impl.SpecializationImpl#getParent <em>Parent</em>}</li>
 *   <li>{@link case_.impl.SpecializationImpl#getChildren <em>Children</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SpecializationImpl extends MinimalEObjectImpl.Container implements Specialization {
	/**
	 * The cached value of the '{@link #getOptionYear() <em>Option Year</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOptionYear()
	 * @generated
	 * @ordered
	 */
	protected Year optionYear;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getParent() <em>Parent</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParent()
	 * @generated
	 * @ordered
	 */
	protected Specialization parent;

	/**
	 * The cached value of the '{@link #getChildren() <em>Children</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChildren()
	 * @generated
	 * @ordered
	 */
	protected EList<Specialization> children;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SpecializationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CasePackage.Literals.SPECIALIZATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Plan getPlan() {
		if (eContainerFeatureID() != CasePackage.SPECIALIZATION__PLAN) return null;
		return (Plan)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPlan(Plan newPlan, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newPlan, CasePackage.SPECIALIZATION__PLAN, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPlan(Plan newPlan) {
		if (newPlan != eInternalContainer() || (eContainerFeatureID() != CasePackage.SPECIALIZATION__PLAN && newPlan != null)) {
			if (EcoreUtil.isAncestor(this, newPlan))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newPlan != null)
				msgs = ((InternalEObject)newPlan).eInverseAdd(this, CasePackage.PLAN__SPECIALIZATIONS, Plan.class, msgs);
			msgs = basicSetPlan(newPlan, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CasePackage.SPECIALIZATION__PLAN, newPlan, newPlan));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Year getOptionYear() {
		if (optionYear != null && optionYear.eIsProxy()) {
			InternalEObject oldOptionYear = (InternalEObject)optionYear;
			optionYear = (Year)eResolveProxy(oldOptionYear);
			if (optionYear != oldOptionYear) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CasePackage.SPECIALIZATION__OPTION_YEAR, oldOptionYear, optionYear));
			}
		}
		return optionYear;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Year basicGetOptionYear() {
		return optionYear;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOptionYear(Year newOptionYear, NotificationChain msgs) {
		Year oldOptionYear = optionYear;
		optionYear = newOptionYear;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CasePackage.SPECIALIZATION__OPTION_YEAR, oldOptionYear, newOptionYear);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOptionYear(Year newOptionYear) {
		if (newOptionYear != optionYear) {
			NotificationChain msgs = null;
			if (optionYear != null)
				msgs = ((InternalEObject)optionYear).eInverseRemove(this, CasePackage.YEAR__SPECIALIZATION_OPTIONS, Year.class, msgs);
			if (newOptionYear != null)
				msgs = ((InternalEObject)newOptionYear).eInverseAdd(this, CasePackage.YEAR__SPECIALIZATION_OPTIONS, Year.class, msgs);
			msgs = basicSetOptionYear(newOptionYear, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CasePackage.SPECIALIZATION__OPTION_YEAR, newOptionYear, newOptionYear));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CasePackage.SPECIALIZATION__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Specialization getParent() {
		if (parent != null && parent.eIsProxy()) {
			InternalEObject oldParent = (InternalEObject)parent;
			parent = (Specialization)eResolveProxy(oldParent);
			if (parent != oldParent) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CasePackage.SPECIALIZATION__PARENT, oldParent, parent));
			}
		}
		return parent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Specialization basicGetParent() {
		return parent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParent(Specialization newParent, NotificationChain msgs) {
		Specialization oldParent = parent;
		parent = newParent;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CasePackage.SPECIALIZATION__PARENT, oldParent, newParent);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setParent(Specialization newParent) {
		if (newParent != parent) {
			NotificationChain msgs = null;
			if (parent != null)
				msgs = ((InternalEObject)parent).eInverseRemove(this, CasePackage.SPECIALIZATION__CHILDREN, Specialization.class, msgs);
			if (newParent != null)
				msgs = ((InternalEObject)newParent).eInverseAdd(this, CasePackage.SPECIALIZATION__CHILDREN, Specialization.class, msgs);
			msgs = basicSetParent(newParent, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CasePackage.SPECIALIZATION__PARENT, newParent, newParent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Specialization> getChildren() {
		if (children == null) {
			children = new EObjectWithInverseResolvingEList<Specialization>(Specialization.class, this, CasePackage.SPECIALIZATION__CHILDREN, CasePackage.SPECIALIZATION__PARENT);
		}
		return children;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CasePackage.SPECIALIZATION__PLAN:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetPlan((Plan)otherEnd, msgs);
			case CasePackage.SPECIALIZATION__OPTION_YEAR:
				if (optionYear != null)
					msgs = ((InternalEObject)optionYear).eInverseRemove(this, CasePackage.YEAR__SPECIALIZATION_OPTIONS, Year.class, msgs);
				return basicSetOptionYear((Year)otherEnd, msgs);
			case CasePackage.SPECIALIZATION__PARENT:
				if (parent != null)
					msgs = ((InternalEObject)parent).eInverseRemove(this, CasePackage.SPECIALIZATION__CHILDREN, Specialization.class, msgs);
				return basicSetParent((Specialization)otherEnd, msgs);
			case CasePackage.SPECIALIZATION__CHILDREN:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getChildren()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CasePackage.SPECIALIZATION__PLAN:
				return basicSetPlan(null, msgs);
			case CasePackage.SPECIALIZATION__OPTION_YEAR:
				return basicSetOptionYear(null, msgs);
			case CasePackage.SPECIALIZATION__PARENT:
				return basicSetParent(null, msgs);
			case CasePackage.SPECIALIZATION__CHILDREN:
				return ((InternalEList<?>)getChildren()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case CasePackage.SPECIALIZATION__PLAN:
				return eInternalContainer().eInverseRemove(this, CasePackage.PLAN__SPECIALIZATIONS, Plan.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CasePackage.SPECIALIZATION__PLAN:
				return getPlan();
			case CasePackage.SPECIALIZATION__OPTION_YEAR:
				if (resolve) return getOptionYear();
				return basicGetOptionYear();
			case CasePackage.SPECIALIZATION__NAME:
				return getName();
			case CasePackage.SPECIALIZATION__PARENT:
				if (resolve) return getParent();
				return basicGetParent();
			case CasePackage.SPECIALIZATION__CHILDREN:
				return getChildren();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CasePackage.SPECIALIZATION__PLAN:
				setPlan((Plan)newValue);
				return;
			case CasePackage.SPECIALIZATION__OPTION_YEAR:
				setOptionYear((Year)newValue);
				return;
			case CasePackage.SPECIALIZATION__NAME:
				setName((String)newValue);
				return;
			case CasePackage.SPECIALIZATION__PARENT:
				setParent((Specialization)newValue);
				return;
			case CasePackage.SPECIALIZATION__CHILDREN:
				getChildren().clear();
				getChildren().addAll((Collection<? extends Specialization>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CasePackage.SPECIALIZATION__PLAN:
				setPlan((Plan)null);
				return;
			case CasePackage.SPECIALIZATION__OPTION_YEAR:
				setOptionYear((Year)null);
				return;
			case CasePackage.SPECIALIZATION__NAME:
				setName(NAME_EDEFAULT);
				return;
			case CasePackage.SPECIALIZATION__PARENT:
				setParent((Specialization)null);
				return;
			case CasePackage.SPECIALIZATION__CHILDREN:
				getChildren().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CasePackage.SPECIALIZATION__PLAN:
				return getPlan() != null;
			case CasePackage.SPECIALIZATION__OPTION_YEAR:
				return optionYear != null;
			case CasePackage.SPECIALIZATION__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case CasePackage.SPECIALIZATION__PARENT:
				return parent != null;
			case CasePackage.SPECIALIZATION__CHILDREN:
				return children != null && !children.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //SpecializationImpl
