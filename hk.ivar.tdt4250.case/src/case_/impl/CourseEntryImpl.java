/**
 */
package case_.impl;

import case_.CasePackage;
import case_.Course;
import case_.CourseEntry;
import case_.CourseGroup;
import case_.CourseStatus;
import case_.SemesterType;
import case_.Specialization;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Course Entry</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link case_.impl.CourseEntryImpl#getGroup <em>Group</em>}</li>
 *   <li>{@link case_.impl.CourseEntryImpl#getCourse <em>Course</em>}</li>
 *   <li>{@link case_.impl.CourseEntryImpl#getAvailableTo <em>Available To</em>}</li>
 *   <li>{@link case_.impl.CourseEntryImpl#getSemester <em>Semester</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CourseEntryImpl extends MinimalEObjectImpl.Container implements CourseEntry {
	/**
	 * The cached value of the '{@link #getCourse() <em>Course</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCourse()
	 * @generated
	 * @ordered
	 */
	protected Course course;

	/**
	 * The cached value of the '{@link #getAvailableTo() <em>Available To</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAvailableTo()
	 * @generated
	 * @ordered
	 */
	protected EMap<CourseStatus, EList<Specialization>> availableTo;

	/**
	 * The default value of the '{@link #getSemester() <em>Semester</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSemester()
	 * @generated
	 * @ordered
	 */
	protected static final SemesterType SEMESTER_EDEFAULT = SemesterType.FALL;

	/**
	 * The cached value of the '{@link #getSemester() <em>Semester</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSemester()
	 * @generated
	 * @ordered
	 */
	protected SemesterType semester = SEMESTER_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CourseEntryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CasePackage.Literals.COURSE_ENTRY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public CourseGroup getGroup() {
		if (eContainerFeatureID() != CasePackage.COURSE_ENTRY__GROUP) return null;
		return (CourseGroup)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGroup(CourseGroup newGroup, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newGroup, CasePackage.COURSE_ENTRY__GROUP, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setGroup(CourseGroup newGroup) {
		if (newGroup != eInternalContainer() || (eContainerFeatureID() != CasePackage.COURSE_ENTRY__GROUP && newGroup != null)) {
			if (EcoreUtil.isAncestor(this, newGroup))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newGroup != null)
				msgs = ((InternalEObject)newGroup).eInverseAdd(this, CasePackage.COURSE_GROUP__COURSES, CourseGroup.class, msgs);
			msgs = basicSetGroup(newGroup, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CasePackage.COURSE_ENTRY__GROUP, newGroup, newGroup));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Course getCourse() {
		if (course != null && course.eIsProxy()) {
			InternalEObject oldCourse = (InternalEObject)course;
			course = (Course)eResolveProxy(oldCourse);
			if (course != oldCourse) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CasePackage.COURSE_ENTRY__COURSE, oldCourse, course));
			}
		}
		return course;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Course basicGetCourse() {
		return course;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCourse(Course newCourse, NotificationChain msgs) {
		Course oldCourse = course;
		course = newCourse;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CasePackage.COURSE_ENTRY__COURSE, oldCourse, newCourse);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCourse(Course newCourse) {
		if (newCourse != course) {
			NotificationChain msgs = null;
			if (course != null)
				msgs = ((InternalEObject)course).eInverseRemove(this, CasePackage.COURSE__PLANS, Course.class, msgs);
			if (newCourse != null)
				msgs = ((InternalEObject)newCourse).eInverseAdd(this, CasePackage.COURSE__PLANS, Course.class, msgs);
			msgs = basicSetCourse(newCourse, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CasePackage.COURSE_ENTRY__COURSE, newCourse, newCourse));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EMap<CourseStatus, EList<Specialization>> getAvailableTo() {
		if (availableTo == null) {
			availableTo = new EcoreEMap<CourseStatus,EList<Specialization>>(CasePackage.Literals.COURSE_STATUS_TO_SPECIALIZATION, CourseStatusToSpecializationImpl.class, this, CasePackage.COURSE_ENTRY__AVAILABLE_TO);
		}
		return availableTo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SemesterType getSemester() {
		return semester;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSemester(SemesterType newSemester) {
		SemesterType oldSemester = semester;
		semester = newSemester == null ? SEMESTER_EDEFAULT : newSemester;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CasePackage.COURSE_ENTRY__SEMESTER, oldSemester, semester));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CasePackage.COURSE_ENTRY__GROUP:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetGroup((CourseGroup)otherEnd, msgs);
			case CasePackage.COURSE_ENTRY__COURSE:
				if (course != null)
					msgs = ((InternalEObject)course).eInverseRemove(this, CasePackage.COURSE__PLANS, Course.class, msgs);
				return basicSetCourse((Course)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CasePackage.COURSE_ENTRY__GROUP:
				return basicSetGroup(null, msgs);
			case CasePackage.COURSE_ENTRY__COURSE:
				return basicSetCourse(null, msgs);
			case CasePackage.COURSE_ENTRY__AVAILABLE_TO:
				return ((InternalEList<?>)getAvailableTo()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case CasePackage.COURSE_ENTRY__GROUP:
				return eInternalContainer().eInverseRemove(this, CasePackage.COURSE_GROUP__COURSES, CourseGroup.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CasePackage.COURSE_ENTRY__GROUP:
				return getGroup();
			case CasePackage.COURSE_ENTRY__COURSE:
				if (resolve) return getCourse();
				return basicGetCourse();
			case CasePackage.COURSE_ENTRY__AVAILABLE_TO:
				if (coreType) return getAvailableTo();
				else return getAvailableTo().map();
			case CasePackage.COURSE_ENTRY__SEMESTER:
				return getSemester();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CasePackage.COURSE_ENTRY__GROUP:
				setGroup((CourseGroup)newValue);
				return;
			case CasePackage.COURSE_ENTRY__COURSE:
				setCourse((Course)newValue);
				return;
			case CasePackage.COURSE_ENTRY__AVAILABLE_TO:
				((EStructuralFeature.Setting)getAvailableTo()).set(newValue);
				return;
			case CasePackage.COURSE_ENTRY__SEMESTER:
				setSemester((SemesterType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CasePackage.COURSE_ENTRY__GROUP:
				setGroup((CourseGroup)null);
				return;
			case CasePackage.COURSE_ENTRY__COURSE:
				setCourse((Course)null);
				return;
			case CasePackage.COURSE_ENTRY__AVAILABLE_TO:
				getAvailableTo().clear();
				return;
			case CasePackage.COURSE_ENTRY__SEMESTER:
				setSemester(SEMESTER_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CasePackage.COURSE_ENTRY__GROUP:
				return getGroup() != null;
			case CasePackage.COURSE_ENTRY__COURSE:
				return course != null;
			case CasePackage.COURSE_ENTRY__AVAILABLE_TO:
				return availableTo != null && !availableTo.isEmpty();
			case CasePackage.COURSE_ENTRY__SEMESTER:
				return semester != SEMESTER_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (semester: ");
		result.append(semester);
		result.append(')');
		return result.toString();
	}

} //CourseEntryImpl
