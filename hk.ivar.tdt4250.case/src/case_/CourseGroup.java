/**
 */
package case_;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Course Group</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link case_.CourseGroup#getYear <em>Year</em>}</li>
 *   <li>{@link case_.CourseGroup#getName <em>Name</em>}</li>
 *   <li>{@link case_.CourseGroup#getCourses <em>Courses</em>}</li>
 * </ul>
 *
 * @see case_.CasePackage#getCourseGroup()
 * @model
 * @generated
 */
public interface CourseGroup extends EObject {
	/**
	 * Returns the value of the '<em><b>Year</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link case_.Year#getCourseGroups <em>Course Groups</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Year</em>' container reference.
	 * @see #setYear(Year)
	 * @see case_.CasePackage#getCourseGroup_Year()
	 * @see case_.Year#getCourseGroups
	 * @model opposite="courseGroups" transient="false"
	 * @generated
	 */
	Year getYear();

	/**
	 * Sets the value of the '{@link case_.CourseGroup#getYear <em>Year</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Year</em>' container reference.
	 * @see #getYear()
	 * @generated
	 */
	void setYear(Year value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see case_.CasePackage#getCourseGroup_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link case_.CourseGroup#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Courses</b></em>' containment reference list.
	 * The list contents are of type {@link case_.CourseEntry}.
	 * It is bidirectional and its opposite is '{@link case_.CourseEntry#getGroup <em>Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Courses</em>' containment reference list.
	 * @see case_.CasePackage#getCourseGroup_Courses()
	 * @see case_.CourseEntry#getGroup
	 * @model opposite="group" containment="true"
	 * @generated
	 */
	EList<CourseEntry> getCourses();

} // CourseGroup
